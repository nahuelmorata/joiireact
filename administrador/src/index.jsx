import React from "react";
import { createRoot } from 'react-dom/client';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';

import "./style.scss";
import logo from "./app/vistas/assets/LogoJOII.png";

import Contexto from "./app/store";
//Vistas
import Login from "./app/vistas/Login";
import Home from "./app/vistas/Home";
import AgregarSeminario from "./app/vistas/AgregarSeminario";
import AgregarInscripto from "./app/vistas/AgregarInscripto";
import AgregarVisita from "./app/vistas/AgregarVisita";
import VerInscriptos from "./app/vistas/VerInscriptos";
import VerSeminarios from "./app/vistas/VerSeminarios";
import VerVisitas from "./app/vistas/VerVisitas";
import listaInscriptosVisitas from "./app/vistas/listaInscriptosVisitas";
import InscriptosUniversidad from "./app/vistas/InscriptosUniversidad";
import ListaCodigos from './app/vistas/ListaCodigos';
import listaGrupos from "./app/vistas/listaGrupos";

const container = document.getElementById('app');
const root = createRoot(container);
root.render(
	<div className="contenedor-centro">
		<img src={logo} />
		<Contexto>
		<Router>
			<Switch>
				<Route path="/" exact component={Login} />
				<Route path="/home" exact component={Home} />
				<Route path="/agregarSeminario" exact component={AgregarSeminario} />
				<Route path="/agregarInscripto" exact component={AgregarInscripto} />
				<Route path="/agregarVisita" exact component={AgregarVisita} />
				<Route path="/verinscriptos" exact component={VerInscriptos} />
				<Route path="/verseminarios" exact component={VerSeminarios} />
				<Route path="/vervisitas" exact component={VerVisitas} />
				<Route path="/inscriptosVisitas" exact component={listaInscriptosVisitas} />
				<Route path="/inscriptosUniversidad" exact component={InscriptosUniversidad} />
				<Route path="/codigos" exact component={ListaCodigos} />
				<Route path="/grupos" exact component={listaGrupos} />
			</Switch>
		</Router>
		</Contexto>
		<p>Creado por <a href="mailto:contacto@nahuelmorata.com.ar" className="nada" data-tip="contacto@nahuelmorata.com.ar">Nahuel Morata</a> y <a href="mailto:charlie.jye@gmail.com" className="nada" data-tip="charlie.jye@gmail.com">Carlos Fernandez</a>.</p>
		<ReactTooltip place="top" type="dark" effect="solid"/>
	</div>
)
