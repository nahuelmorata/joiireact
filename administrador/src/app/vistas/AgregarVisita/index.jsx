import React,{ Component } from 'react';
import { render } from 'react-dom';
import { Redirect,Link } from "react-router-dom";

import "./style.scss";
import {Consumidor} from "../../store";
import {agregarVisita} from '../clienteGraphql.admin.js';

class AgregarSeminario extends Component{
	constructor(props){
		super(props);
		this.state = {
			titulo:'',
			cant:0,
			hora:"",
			fecha:"",
			lugar:"",
			agregarCorrecto:false
		}
		this.actualizarNombre = this.actualizarNombre.bind(this);
		this.actualizarCantidad = this.actualizarCantidad.bind(this);
		this.actualizarLugar = this.actualizarLugar.bind(this);
		this.actualizarFecha = this.actualizarFecha.bind(this);
		this.actualizarHora = this.actualizarHora.bind(this);
		this.click = this.click.bind(this);
	}
	actualizarNombre(e){
		this.setState({titulo:event.target.value});
	}
	actualizarCantidad(e){
		this.setState({cant:event.target.value});
	}
	actualizarFecha(e){
		this.setState({fecha:event.target.value});
	}
	actualizarHora(e){
		this.setState({hora:event.target.value});
	}
	actualizarLugar(e){
		this.setState({lugar:event.target.value});
	}
	click(){
		var token = this.props.context.estado.token;
		var titulo = this.state.titulo;
		var lugar = this.state.lugar;
		var fecha = this.state.fecha;
		var hora = this.state.hora;
		var cant = this.state.cant;
		agregarVisita(titulo,lugar,fecha,hora,cant).then(resultado=>{
			this.setState({agregarCorrecto:true});
		});
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		if (this.state.agregarCorrecto){
			return <Redirect to="/home" />
		}
		return(
		<div className="home">
			<h1>Agregar Visita</h1>
			<div className="fila">
				<input type="text" placeholder="Titulo" onChange={this.actualizarNombre} />
			</div>
			<div className="fila">
				<input type="date" placeholder="Fecha y hora" format="YYYY MMMM DD" onChange={this.actualizarFecha} />
				<input type="time" step="900" onChange={this.actualizarHora} />
			</div>
			<div className="fila">
				<input type="text" placeholder="Lugar" onChange={this.actualizarLugar} />
			</div>
			<div className="fila">
				<input type="number" placeholder="Cantidad de cupo para Inscriptos" onChange={this.actualizarCantidad} />
			</div>
			<div className="fila">
				<button onClick={()=>this.click()}> Agregar </button><Link to="/home"> Volver </Link>
			</div>
		</div>
		);
	}
}

export default Consumidor(AgregarSeminario);
