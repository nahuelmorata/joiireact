
import React,{ Component } from 'react';
import {Link} from 'react-router-dom';
import ReactToPrint from 'react-to-print';
import {Consumidor} from "../../store";
import {filtroInscriptoUniversidad,visitas} from '../clienteGraphql.admin.js';

import "./style.scss";

class InscriptosUniversidad extends Component{
    constructor(props){
        super(props);
        this.state ={
            universidades: [
                {nombre:'UNS',id:1},
                {nombre:'UTN',id:2},
                {nombre:'Otros',id:3},
            ],
            universidadActiva:{},
            eleccion: 0,
            listaActiva: []
        }
        this.cambioLista = this.cambioLista.bind(this);
    }
    cambioLista(event){
        let lista = [];
        let value = event.target.value;
        let token = this.props.context.estado.token;
        this.setState({eleccion:value});
        filtroInscriptoUniversidad(token,value).then((resultado)=>{
            lista = resultado;
        }).then(()=>{
            let index = this.state.universidades.findIndex((universidad)=>{
                return universidad.id == value;
            })
            this.setState({listaActiva:lista,universidadActiva:this.state.universidades[index]});
            console.log(this.state.universidadActiva);
        })
    }
    render(){
        return (
            <div className="home">
                <select value={this.state.eleccion} onChange={this.cambioLista}>
                    <option value="0">Elije una Universidad para ver la Lista</option>
                    {
                        this.state.universidades.map((visita,key)=>{
                            return <option key={key} value={visita.id}>{visita.nombre}</option>
                        })
                    }
                </select>
                <div className="lista" ref={el => (this.componentRef = el)}>
                    <ul>
                        <li><h1>{this.state.universidadActiva.nombre}</h1></li>
                        {
                            this.state.listaActiva.map((inscripto,key)=>{
                                return <li key={key}>{inscripto.apellidos}, {inscripto.nombres} - {inscripto.dni}</li>
                            })
                        }
                    </ul>
                </div>
                <div className="fila">
                    <ReactToPrint
		                trigger={() => <button>Imprimir</button>}
		                content={() => this.componentRef}
		            />
                    <Link to="/home">Volver</Link>
                </div>
            </div>
        );
    }
}

export default Consumidor(InscriptosUniversidad);