import React,{ Component } from 'react';
import { render } from 'react-dom';
import { Redirect,Link } from "react-router-dom";

import "./style.scss";
import {Consumidor} from "../../store";
import {inscriptos} from '../clienteGraphql.admin.js';
import ItemInscripto from "../componentes/ItemInscripto";

class VerInscriptos extends Component{
	constructor(props){
		super(props);
		this.state = {
			inscriptoActual: null,
			inscriptos: [],
			log: false
		}
	}
	componentDidMount(){
		inscriptos().then(resultado=>{
			let lista = [];
			resultado.items.map(inscripto=>{
				if (inscripto.dni !== 0){
					lista.push(inscripto);
				}
			});
			this.setState({inscriptos: lista});
		})
	}
	render() {
		if (this.state.log){
			return <Redirect to="/home" />
		}
		return(
		<div className="home">
			<h1>Inscriptos</h1>
			<div className="lista">
			{
				this.state.inscriptos.map((inscripto,key)=>
					<ItemInscripto key={key} inscripto={inscripto} />
				)
			}
			</div>
			<div className="fila">
				<Link to="/home"> Volver </Link>
			</div>
		</div>
		);
	}
}

export default Consumidor(VerInscriptos);
