import React,{ Component } from 'react';
import { render } from 'react-dom';
import { Redirect,Link } from "react-router-dom";

import "./style.scss";
import {Consumidor} from "../../store";

class Home extends Component{
	constructor(props){
		super(props);
		this.state = {
			titulo: ''
		}
	}
	componentDidMount(){
		let value = '';
		switch(this.props.context.estado.nivel){
			case 2:
				value = "Grupo Academicas";
				break;
			case 3:
				value = "Grupo Visitas";
				break;
			case 4:
				value = "Grupo Inscripciones";
				break;
			case 1:
				value = "Dioses Desarrolladores de su Sistema.";
				break;
		}
		this.setState({titulo:value});
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		let items = [];
		
		if (this.props.context.estado.nivel === 2) {
			items.push(
				<div key={1} className="parte">
					<h1>Seminarios</h1>
					<div className="contenido">
						<Link to="/agregarSeminario"> Agregar Seminario </Link>
						<Link to="/verSeminarios"> Ver Seminarios </Link>
					</div>
				</div>
			);
		} else if (this.props.context.estado.nivel === 3) {
			items.push(
				<div key={2} className="parte">
					<h1>Visitas</h1>
					<div className="contenido">
						<Link to="/agregarVisita"> Agregar Visita </Link>
						<Link to="/vervisitas"> Ver Visitas </Link>
					</div>
				</div>
			);
		} else if (this.props.context.estado.nivel === 4) {
			items.push(
				<div key={0} className="parte">
					<h1>Inscriptos</h1>
					<div className="contenido">
						<Link to="/agregarInscripto"> Agregar Inscripto </Link>
						<Link to="/verInscriptos"> Ver Inscriptos </Link>
					</div>
				</div>
			);
		} else if (this.props.context.estado.nivel === 1){
			items.push(
				<div key={1} className="parte">
					<h1>Seminarios</h1>
					<div className="contenido">
						<Link to="/agregarSeminario"> Agregar Seminario </Link>
						<Link to="/verSeminarios"> Ver Seminarios </Link>
					</div>
				</div>
			);
			items.push(
				<div key={2} className="parte">
					<h1>Visitas</h1>
					<div className="contenido">
						<Link to="/agregarVisita"> Agregar Visita </Link>
						<Link to="/vervisitas"> Ver Visitas </Link>
					</div>
				</div>
			);
			items.push(
				<div key={0} className="parte">
					<h1>Inscriptos</h1>
					<div className="contenido">
						<Link to="/agregarInscripto"> Agregar Inscripto </Link>
						<Link to="/verInscriptos"> Ver Inscriptos </Link>
					</div>
				</div>
			);
		}

		return(
		<div className="home">
			<h3>{this.state.titulo}</h3>
			{ items }
			<div className="parte">
				<h1>Listas</h1>
				<div className="contenido">
					<Link to="/grupos">Grupos</Link>
					<Link to="/codigos">Codigos</Link>
					<Link to="/inscriptosVisitas">Visita</Link>
					<Link to="/inscriptosUniversidad">Universidad</Link>
				</div>
			</div>
		</div>
		);
	}
}

export default Consumidor(Home);
