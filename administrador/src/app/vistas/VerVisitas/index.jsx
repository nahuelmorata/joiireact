import React,{ Component } from 'react';
import { render } from 'react-dom';
import { Redirect,Link } from "react-router-dom";

import "./style.scss";
import {Consumidor} from "../../store";
import {visitas} from '../clienteGraphql.admin.js';
import ItemVisitas from '../componentes/ItemVisitas';

class AgregarVisitas extends Component{
	constructor(props){
		super(props);
		this.state = {
			seminarioActual:null,
			listaVisitas:[]
		}
		this.click = this.click.bind(this);
		this.actualizar = this.actualizar.bind(this);
		this.montado = true;
	}

	click(){
		console.log('apreto');
	}
	componentDidMount(){
		visitas().then(resultado=>{
			if (this.montado){
				this.setState({listaVisitas:resultado});
			}	
		});
	}
	actualizar(){
		console.log('cargando');
		visitas().then(resultado=>{
			if (this.montado){
				this.setState({listaVisitas:[]});
				this.setState({listaVisitas:resultado});
				console.log('lo borro');
			}
		});
	}
	componentWillUnmount(){
		this.montado = false;
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		return(
		<div className="home">
			<h1>Visitas</h1>
			<div className="lista">
				{
					this.state.listaVisitas.map((visita,key)=>
						<ItemVisitas key={key} visita={visita} actualizar={this.actualizar} />
					)
				}
			</div>
			<div className="fila">
				<Link to="/home"> Volver </Link>
			</div>
		</div>
		);
	}
}

export default Consumidor(AgregarVisitas);
