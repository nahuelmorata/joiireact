import React,{ Component } from 'react';
import { Redirect } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

import "./style.scss";
import 'react-toastify/dist/ReactToastify.css';
import {Consumidor} from "../../store";
import {login} from '../clienteGraphql.admin.js';

class Login extends Component{
	constructor(props){
		super(props);
		this.state = {
			documento: null,
			codigo: null
		}
		this.cambioUser = this.cambioUser.bind(this);
		this.cambioPass = this.cambioPass.bind(this);
		this.click = this.click.bind(this);
	}
	cambioUser(event){
		this.setState({documento:event.target.value});
	}
	cambioPass(event){
		this.setState({codigo:event.target.value});
	}
	click(){
		const context = this.props.context;
		login(this.state.documento,this.state.codigo).then(authResponse=>{
			if (authResponse?.token == null) {
				toast('Error de usuario y/o contraseña');
				return;
			}
			context.acciones.login(authResponse.token, authResponse.usuario.rol.id);
		});
	}
	render() {
		if (this.props.context.estado.token !== "") {
			return <Redirect to="/home" />
		}
		return(
			<div className="cuadro">
				<h1>Administracion</h1>
				<input type="text" name="documento" placeholder="User" onChange={this.cambioUser} />
				<input type="password" name="codigo" placeholder="Password" onChange={this.cambioPass} />
				<button onClick={()=>{ this.click() }}><i className="fas fa-address-book"></i> Loguearse</button>
				<ToastContainer position={toast.POSITION.TOP_RIGHT} autoClose={2000} />
			</div>
			);
	}
}

export default Consumidor(Login);
