import React,{ Component } from 'react';
import { render } from 'react-dom';

import "./style.scss";
import {modificarVisita,borrarVisita} from '../../clienteGraphql.admin.js';

class VerVisitas extends Component{
	constructor(props){
		super(props);
		this.state={
			nombre:this.props.visita.nombre,
			cupo:this.props.visita.cupo,
			id:this.props.visita.id,
			ocupado:this.props.visita.ocupado,
			lugar:this.props.visita.lugar,
			fecha: this.props.visita.fechaHora.split('T')[0],
			hora: this.props.visita.fechaHora.split('T')[1].split('-')[0],
			editando:false
		}
		this.borrar = this.borrar.bind(this);
		this.modoEditar = this.modoEditar.bind(this);
		this.manejoDisplay = this.manejoDisplay.bind(this);
		this.ModificarVisita = this.ModificarVisita.bind(this);
		this.myRef = React.createRef();
		this.cambioNombre = this.cambioNombre.bind(this);
		this.cambioCupo = this.cambioCupo.bind(this);
		this.cambioLugar = this.cambioLugar.bind(this);
		this.cambioFecha = this.cambioFecha.bind(this);
		this.cambioHora = this.cambioHora.bind(this);
		this.actualizar = this.props.actualizar
	}
	modoEditar(){
		this.setState({editando:!this.state.editando});
		setTimeout(this.manejoDisplay(), 800);
	}
	manejoDisplay(){
		const node = this.myRef.current;
		if (this.state.editando){
			node.style.display = "flex";
		} else {
			node.style.display = "none";
		}
	}
	cambioNombre(event){
		this.setState({nombre:event.target.value});
	}
	cambioCupo(event){
		this.setState({cupo:event.target.value});
	}
	cambioLugar(event){
		this.setState({lugar:event.target.value});
	}
	cambioFecha(event){
		this.setState({fecha:event.target.value});
	}
	cambioHora(event){
		this.setState({hora:event.target.value});
	}
	ModificarVisita(){
		modificarVisita(this.state.id,this.state.nombre,this.state.lugar,this.state.fecha,this.state.hora,this.state.cupo).then(()=>{
			this.actualizar();
			this.modoEditar();
		});
	}
	borrar(){
		borrarVisita(this.state.id).then(() => {
			this.props.actualizar();
		});
	}
	render(){
		return(
			<div className="item">
				<div>
					{this.state.nombre}({this.state.cupo})
				</div>
				<div id="botonera">
					<button onClick={this.modoEditar}><i className="fas fa-pencil-alt"></i></button>
					<button onClick={this.borrar}><i className="fas fa-trash-alt"></i></button>
				</div>
				<div className="modal" ref={this.myRef}>
				<div className="home">
						<h1>Editar Visita</h1>
						<div className="fila">
							<input type="text" value={this.state.nombre} onChange={this.cambioNombre} />
						</div>
						<div className="fila">
							<input type="text" value={this.state.lugar} onChange={this.cambioLugar} />
						</div>
						<div className="fila">
							<input type="date" format="YYYY MMMM DD" value={this.state.fecha} onChange={this.cambioFecha} />
							<input type="time" step="900" value={this.state.hora} onChange={this.cambioHora} />
						</div>
						<div className="fila">
							<input type="number" value={this.state.cupo} onChange={this.cambioCupo} />
						</div>
						<div className="fila">
							<button onClick={this.ModificarVisita}>Guardar Cambios</button><button onClick={this.modoEditar} >Volver</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default VerVisitas;
