import React,{ Component } from 'react';
import { render } from 'react-dom';
import { Redirect,Link } from "react-router-dom";

import "./style.scss";
import {Consumer} from "../../../store";


class Home extends Component{
	constructor(props){
		super(props);
		this.state = {
			token: this.props.token
		}
	}
	render() {
		if (this.props.token == ""){
			return <Redirect to="/" />
		}
		return(
			<div className="home">
				contenido {this.props.token}
			</div>
			);
	}
}

export default Home;