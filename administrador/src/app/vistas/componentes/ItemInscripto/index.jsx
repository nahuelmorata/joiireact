import React,{ Component } from 'react';
import { render } from 'react-dom';

import "./style.scss";

class ItemInscripto extends Component{
	constructor(props){
		super(props);
		this.state={
			nombre:this.props.inscripto.nombres,
			apellido:this.props.inscripto.apellidos,
			documento:this.props.inscripto.dni,
			id:this.props.inscripto.id
		}
	}
	render(){
		return(
			<div className="item">
				<div>
					{this.state.nombre} {this.state.apellido} - {this.state.documento}
				</div>
			</div>
		);
	}
}
export default ItemInscripto;