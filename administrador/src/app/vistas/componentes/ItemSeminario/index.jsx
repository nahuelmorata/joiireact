import React,{ Component } from 'react';
import { render } from 'react-dom';

import "./style.scss";
import {modificarSeminario,borrarSeminario} from '../../clienteGraphql.admin.js';

class ItemSeminario extends Component{
	constructor(props){
		super(props);
		this.state={
			nombre:this.props.seminario.nombre,
			cantOpcional:this.props.seminario.cupoOpcional,
			id:this.props.seminario.id,
			cantObligatorio:this.props.seminario.cupoObligatorio,
			ocupadoObligatorio: this.props.seminario.ocupadoObligatorio,
			ocupadoOpcional: this.props.seminario.ocupadoOpcional,
			fecha: this.props.seminario.fechaHora.split('T')[0],
			hora: this.props.seminario.fechaHora.split('T')[1].split('-')[0],
			lugar: this.props.seminario.lugar,
			editando:false
		}
		this.borrar = this.borrar.bind(this);
		this.modoEditar = this.modoEditar.bind(this);
		this.manejoDisplay = this.manejoDisplay.bind(this);
		this.cambioNombre = this.cambioNombre.bind(this);
		this.cambioLugar = this.cambioLugar.bind(this);
		this.cambioObligatorio = this.cambioObligatorio.bind(this);
		this.cambioOpcional = this.cambioOpcional.bind(this);
		this.cambioHora = this.cambioHora.bind(this);
		this.cambioFecha = this.cambioFecha.bind(this);
		this.ModificarSeminario = this.ModificarSeminario.bind(this);
		this.actualizar = this.props.actualizar;
		this.myRef = React.createRef();
	}
	modoEditar(){
		console.log("mostrar");
		this.setState({editando:!this.state.editando});
		setTimeout(this.manejoDisplay(), 800);
	}
	manejoDisplay(){
		const node = this.myRef.current;
		if (this.state.editando){
			node.style.display = "flex";
		} else {
			node.style.display = "none";
		}
	}
	cambioOpcional(event){
		this.setState({cantOpcional:event.target.value});
	}
	cambioObligatorio(event){
		this.setState({cantObligatorio:event.target.value});
	}
	cambioNombre(event){
		this.setState({nombre:event.target.value});
	}
	cambioLugar(event){
		this.setState({lugar:event.target.value});
	}
	cambioFecha(event){
		this.setState({fecha:event.target.value});
	}
	cambioHora(event){
		this.setState({hora:event.target.value});
	}
	ModificarSeminario(){
		modificarSeminario(this.state.id, this.state.nombre, this.state.lugar, this.state.fecha, this.state.hora, this.state.cantObligatorio, this.state.cantOpcional).then((resultado)=>{
			this.actualizar();
			this.modoEditar();
		});
	}
	borrar(){
		borrarSeminario(this.state.id).then(() => {
			this.props.actualizar();
		});
	}
	render(){
		return(
			<div className="item">
				<div>
					{this.state.nombre}({this.state.cantObligatorio}/{this.state.cantOpcional})
				</div>
				<div id="botonera">
					<button onClick={this.modoEditar}><i className="fas fa-pencil-alt"></i></button>
					<button onClick={this.borrar}><i className="fas fa-trash-alt"></i></button>
				</div>
				<div className="modal" ref={this.myRef}>
					<div className="home">
						<h1>Editar Seminario</h1>
						<div className="fila">
							<input type="text" value={this.state.nombre} onChange={this.cambioNombre} />
						</div>
						<div className="fila">
							<input type="text" value={this.state.lugar} onChange={this.cambioLugar} />
						</div>
						<div className="fila">
							<input type="date" format="YYYY MMMM DD" value={this.state.fecha} onChange={this.cambioFecha} />
							<input type="time" step="900" value={this.state.hora} onChange={this.cambioHora} />
						</div>
						<div className="fila">
							<input type="number" value={this.state.cantObligatorio} onChange={this.cambioObligatorio} />
							<input type="number" value={this.state.cantOpcional} onChange={this.cambioOpcional} />
						</div>
						<div className="fila">
							<button onClick={this.ModificarSeminario}>Guardar Cambios</button><button onClick={this.modoEditar} >Volver</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default ItemSeminario;
