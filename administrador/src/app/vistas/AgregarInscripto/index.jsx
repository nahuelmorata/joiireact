 import React,{ Component } from 'react';
import { render } from 'react-dom';
import { Redirect,Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';

import "./style.scss";
import 'react-toastify/dist/ReactToastify.min.css';
import {Consumidor} from "../../store";
import {agregarInscripto,contadores} from '../clienteGraphql.admin.js';
import QrReader from 'react-qr-reader';

class AgregarInscripto extends Component{
	constructor(props){
		super(props);
		this.state = {
			codigo:'',
			documento:'',
			agregoCorrecto:false,
			camara:false,
			contador1: 0,
			contador2: 0,
		}
		this.actualizarDocumento = this.actualizarDocumento.bind(this);
		this.actualizarCodigo = this.actualizarCodigo.bind(this);
		this.click = this.click.bind(this);
		this.abrirQR = this.abrirQR.bind(this);
		this.manejandoScan = this.manejandoScan.bind(this);
		this.errorScan = this.errorScan.bind(this);
	}
	actualizarDocumento(e){
		this.setState({documento:event.target.value});
	}
	actualizarCodigo(e){
		this.setState({codigo:event.target.value});
	}
	click(){
		var documento = this.state.documento;
		var codigo = this.state.codigo;
		agregarInscripto(codigo,documento).then(resultado=>{
			if (resultado != null) {
				toast(resultado);
				return;
			}
			toast("Se agrego correctamente", {
				onClose: () => {
					this.setState({agregoCorrecto:true});
				}
			});
		})
	}
	manejandoScan(data){
		if (data){
			console.log(data);
		}
	}
	errorScan(err){
		console.error(err);
	}
	abrirQR(){
		this.setState({camara:!this.state.camara});
	}
	componentDidMount(){
		contadores().then(resultado=>{
			this.setState({contador1:resultado.global,contador2:resultado.dia});
		})
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		if (this.state.agregoCorrecto){
			return <Redirect to="/home" />
		}
		return(
		<div className="home">
			<div className="contadores">
				<p><b>Global: </b>{this.state.contador1}<b> Dia: </b>{this.state.contador2}</p>
			</div>
			{ this.state.camara ? <div id='codigo'><QrReader delay={300} onError={this.errorScan} onScan={this.manejandoScan} /></div> : null }
			<h1>Agregar Inscripto</h1>
			<div className="fila">
				<input type="text" placeholder="Documento del inscripto" onChange={this.actualizarDocumento} />
			</div>
			<div className="fila">
				<input type="text" placeholder="Codigo del papel" onChange={this.actualizarCodigo} /><button><i className="fas fa-qrcode"></i></button>
			</div>
			<div className="fila">
				<button onClick={()=>this.click()}> Agregar </button><Link to="/home"> Volver </Link>
			</div>
			<ToastContainer position={toast.POSITION.TOP_RIGHT} autoClose={2000}/>
		</div>
		);
	}
}

export default Consumidor(AgregarInscripto);
