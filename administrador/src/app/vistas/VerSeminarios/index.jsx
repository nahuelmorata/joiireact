import React,{ Component } from 'react';
import { render } from 'react-dom';
import { Redirect,Link } from "react-router-dom";

import "./style.scss";
import {Consumidor} from "../../store";
import {seminarios} from '../clienteGraphql.admin.js';
import ItemSeminario from "../componentes/ItemSeminario";

class VerSeminario extends Component{
	constructor(props){
		super(props);
		this.state = {
			seminarioActual:null,
			listaSeminarios:[]
		}
		this.click = this.click.bind(this);
		this.actualizar = this.actualizar.bind(this);
	}

	click(){
	}
	actualizar(){
		seminarios().then(resultado=>{
			this.setState({listaSeminarios:[]});
			this.setState({listaSeminarios:resultado});
		});
	}
	componentDidMount(){
		seminarios().then(resultado=>{
			this.setState({listaSeminarios: resultado});
		});
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		return(
		<div className="home">
			<h1>Seminarios</h1>
			<div className="lista">
			{
				this.state.listaSeminarios.map((seminario,key)=>
					<ItemSeminario seminario={seminario} key={key} actualizar={this.actualizar} token={this.props.context.estado.token} />
				)
			}
			</div>
			<div className="fila">
				<Link to="/home"> Volver </Link>
			</div>
		</div>
		);
	}
}

export default Consumidor(VerSeminario);
