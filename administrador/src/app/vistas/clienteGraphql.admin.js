const url = "http://localhost:5000";

/**
 * @typedef {Object} Respuesta - Representa la respuesta
 * @property {number} error - Error de la respuesta 
 * @property {string} token - Token de la cuenta
 * @property {number} nivel - Nivel de la cuenta
 */

/**
 * @typedef {Object} Rol - Representa un rol
 * @property {number} id - Id del rol
 * @property {string} nombre - Nombre del rol
 */

/**
 * @typedef {Object} Usuario - Representa un usuario
 * @property {number} id - Id del usuario
 * @property {string} user - Usuario
 * @property {string} nombres - Nombres del usuario
 * @property {string} apellidos - apellidos del usuario
 * @property {Rol} rol - Rol del usuario
 */

/**
 * @typedef {Object} AuthResponse - Representa la respuesta del login
 * @property {string} token - Token de la cuenta
 * @property {Usuario} usuario - Usuario de la cuenta
 */

/**
 * @typedef {Object} Seminario - Representa un seminario
 * @property {number} id - Id del seminario
 * @property {string} nombre - Nombre del seminario
 * @property {string} lugar - Lugar del seminario
 * @property {string} fechaHora - Fecha y hora del seminario en formato (año-mes-dia hora:minutos:segundos)
 * @property {number} cupoObligatorio - Cupo obligatorio del seminario
 * @property {number} ocupadoObligatorio - Parte ocupada del obligatorio del seminario
 * @property {number} cupoOpcional - Cupo opcional del seminario
 * @property {number} ocupadoOpcional - Parte ocupada del opcional del seminario
 */

/**
 * @typedef {Object} Visita - Representa un visita
 * @property {number} id - Id de la visita
 * @property {string} nombre - Nombre de la visita
 * @property {string} lugar - Lugar de la visita
 * @property {string} fechaHora - Fecha y hora de la visita en formato (año-mes-dia hora:minutos:segundos)
 * @property {number} cupo - Cupo de la visita
 * @property {number} ocupado - Parte ocupada de la visita
 */

/**
 * @typedef {Object} Muestra - Representa una muestra
 * @property {number} id - Id de la muestra
 * @property {string} hora - Hora de la muestra en formato (hora:minutos:segundos)
 * @property {number} cupo - Cupo de la muestra
 * @property {number} ocupado - Parte ocupada de la muestra
 */

/**
 * @typedef {Object} Alumno - Representa datos de un alumno
 * @property {number} id - Id del alumno
 * @property {string} apellidos - Apellidos del alumno
 * @property {string} nombres - Nombres del alumno
 * @property {number} dni - DNI del alumno
 */

 /**
 * @typedef {Object} RespuestaCodigo - Representa la respuesta del codigo
 * @property {number} codigo - Codigo de un inscripto
 */

/**
 * @typedef {Object} Grupo - Representa un grupo
 * @property {number} id - Id del grupo
 * @property {string} nombre - Nombre del grupo
 */

/**
 * @typedef {Object} InscriptoGrupo - Representa un inscripto de un grupo
 * @property {number} id - Id del inscripto
 * @property {string} nombres - Nombres del inscripto
 * @property {string} apellidos - Apellidos del inscripto
 * @property {number} celular - Celular del inscripto
 */

/**
 * @typedef {Object} GruposRespuesta - Representa la respuesta del grupo de admin
 * @property {Grupo} grupo - Datos del grupo
 * @property {InscriptoGrupo[]} inscripto - Inscriptos del grupo
 */

/**
 * @typedef {Object} Contadores - Representa los contadores de inscriptos
 * @property {number} global - Contador global de inscriptos
 * @property {number} dia - Contador diario de inscriptos
 */

/**
 * @typedef {Object} Lista<T> - Representa lista de entidades
 * @property {T[]} items - Lista de entidades
 * @property {number} cantidad - Cantidad de items
 */

/**
 * Funcion de fetch configurado para hacer request a la api
 *
 * @param url Url del endpoint
 * @param metodo Metodo del endpoint
 * @param dto DTO a enviar
 * @returns {Promise<any>}
 */
const fetchApi = (url, metodo, dto) => {
    return fetch(url, {
        method: metodo,
        mode: 'cors',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem("token")}`,
            'Content-Type': 'application/json',
        },
        body: dto == null ? null : JSON.stringify(dto)
    }).then((response) => {
        if (response.status === 401) {
            localStorage.removeItem("token");
            localStorage.removeItem("nivel");
            location.reload();
            return;
        }
        if (response.status === 400) {
            throw new Error(response.statusText);
        }
        if (metodo === 'DELETE') {
            return null;
        }
        return response.json().catch(() => null);
    }).catch(() => {
    });
}

export function obtenerEstado() {
    void fetchApi(`${url}/estado`, 'GET', null);
}

/**
 * Agrega un inscripto
 *
 * @param {number} codigo - Codigo para inscripto
 * @param {number} dni - DNI del inscripto
 * @returns {Promise<string | null>} Promisa con el error o nulo en el caso de todo correcto
 */
export function agregarInscripto(codigo, dni) {
    return fetchApi(`${url}/inscriptos`, 'POST', {
        codigo: codigo,
        dni: dni
    }).then(() => null).catch((ex) => ex.message);
}

/**
 * Loguea un usuario devolviendo un token
 * 
 * @param {string} usuario 
 * @param {string} password 
 * @returns {Promise<AuthResponse>} Promisa con la respuesta
 */
export function login(usuario, password) {
    return fetch(`${url}/auth/login`, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            usuario: usuario,
            password: password
        })
    }).then((response) => {
        if (response.status === 400) {
            return null;
        }
        return response.json();
    }).catch(() => {});
}

/**
 * Filtro para obtener todos los inscriptos por seminario obligatorio
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} seminarioId - Id del seminario obligatorio
 * @returns {Promise<Alumno[]>} Promisa con un array de alumnos
 */
export function filtroInscriptoSeminarioObligatorio(token, seminarioId) {
    const filtroInscriptoSeminarioQuery = `
        query FiltroInscriptoSeminario($token: String!, $seminarioId: Int!) {
            inscripto(token: $token, filtro: 0, seminarioFiltrar: $seminarioId) {
                id
                apellidos
                nombres
                dni
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: filtroInscriptoSeminarioQuery,
            variables: {
                token: token,
                seminarioId: seminarioId
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.inscripto;
    });
}

/**
 * Filtro para obtener todos los inscriptos por seminario opcional
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} seminarioId - Id del seminario obligatorio
 * @returns {Promise<Alumno[]>} Promisa con un array de alumnos
 */
export function filtroInscriptoSeminarioOpcional(token, seminarioId) {
    const filtroInscriptoSeminarioQuery = `
        query FiltroInscriptoSeminario($token: String!, $seminarioId: Int!) {
            inscripto(token: $token, filtro: 1, seminarioFiltrar: $seminarioId) {
                id
                apellidos
                nombres
                dni
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: filtroInscriptoSeminarioQuery,
            variables: {
                token: token,
                seminarioId: seminarioId
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.inscripto;
    });
}

/**
 * Filtro para obtener todos los inscriptos por visita
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} visitaId - Id de la visita
 * @returns {Promise<Alumno[]>} Promisa con un array de alumnos
 */
export function filtroInscriptoVisita(token, visitaId) {
    const filtroInscriptoVisitaQuery = `
        query FiltroInscriptoVisita($token: String!, $visitaId: Int!) {
            inscripto(token: $token, filtro: 2, visitaFiltrar: $visitaId) {
                id
                apellidos
                nombres
                dni
                universidad
                lu
                celular
                carrera
                email
                fechaNac
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: filtroInscriptoVisitaQuery,
            variables: {
                token: token,
                visitaId: visitaId
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.inscripto;
    });
}

/**
 * Filtro para obtener todos los inscriptos por universidad
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} universidadId - Id de la universidad
 * @returns {Promise<Alumno[]>} Promisa con un array de alumnos
 */
export function filtroInscriptoUniversidad(token, universidadId) {
    const filtroInscriptoUniversidadQuery = `
        query FiltroInscriptoUniversidad($token: String!, $universidadId: Int!) {
            inscripto(token: $token, filtro: 3, universidadFiltrar: $universidadId) {
                id
                apellidos
                nombres
                dni
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: filtroInscriptoUniversidadQuery,
            variables: {
                token: token,
                universidadId: universidadId
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.inscripto;
    });
}

/**
 * Obtiene todos los seminarios obligatorios
 * 
 * @returns {Promise<Seminario>} Promisa con un arreglo de todos los seminarios
 */
export function seminarios() {
    return fetchApi(`${url}/seminarios`, 'GET', null).catch(() => {
        return [];
    });
}

/**
 * Agrega un seminario dado su nombre y cupo
 *
 * @param {string} nombre - Nombre del seminario
 * @param {string} lugar - Lugar del seminario
 * @param {string} fecha - Fecha del seminario (año-mes-dia)
 * @param {string} hora - Hora del seminario (hora:minutos:segundos)
 * @param {number} cupoObligatorio - Cupo obligatorio del seminario 
 * @param {number} cupoOpcional - Cupo opcional del seminario 
 * @returns {Promise<Seminario || null>} Promisa que devuelve el mensaje de error o vacio si es correcto
 */
export function agregarSeminario(nombre, lugar, fecha, hora, cupoObligatorio, cupoOpcional) {
    return fetchApi(`${url}/seminarios`, 'POST', {
        nombre: nombre,
        lugar: lugar,
        fechaHora: `${fecha}T${hora}:00-03`,
        cupoObligatorio: cupoObligatorio,
        cupoOpcional: cupoOpcional
    }).catch(() => {
        return null;
    });
}

/**
 * Modifica el seminario dado su id
 * 
 * Codigos de respuesta:
 *  0 si todo es correcto
 *  1 si el nombre del seminario ya existe
 *  2 si no esta logueado
 *  3 si el cupo obligatorio es menor a la cantidad de inscriptos al seminario
 *  4 si el cupo opcional es menor a la cantidad de inscriptos al seminario
 *
 * @param {number} id - Id del seminario a modificar
 * @param {string} nombre - Nombre del seminario
 * @param {string} lugar - Lugar del seminario
 * @param {string} fecha - Fecha del seminario (año-mes-dia)
 * @param {string} hora - Hora del seminario (hora:minutos:segundos)
 * @param {number} cupoObligatorio - Cupo obligatorio del seminario
 * @param {number} cupoOpcional - Cupo opcional del seminario
 * @returns {Promise<number>} Promisa con el codigo de respuesta
 */
export function modificarSeminario(id, nombre, lugar, fecha, hora, cupoObligatorio, cupoOpcional) {
    return fetchApi(`${url}/seminarios/${id}`, 'PUT', {
        id: id,
        nombre: nombre,
        lugar: lugar,
        fechaHora: `${fecha}T${hora}-03`,
        cupoObligatorio: cupoObligatorio,
        cupoOpcional: cupoOpcional
    }).catch(() => null);
}

/**
 * Borra un seminario dado su id
 * 
 * Codigo de respuesta:
 *  0 si es correcto
 *  1 si no esta logueado
 *
 * @param {number} id - Id del seminario
 * @returns {Promise<void>} Promisa con el codigo de respuesta
 */
export function borrarSeminario(id) {
    return fetchApi(`${url}/seminarios/${id}`, 'DELETE', null);
}

/**
 * Agrega una visita dado su nombre y cupo
 * 
 * Codigos de respuesta:
 *  0 si lo agrego correctamente
 *  1 si no esta logueado
 *  2 si el nombre de la visita ya existe
 *
 * @param {string} nombre - Nombre de la visita
 * @param {string} lugar - Lugar de la visita
 * @param {string} fecha - Fecha de la visita(año-mes-dia)
 * @param {string} hora - Hora de la visita(hora:minutos:segundos)
 * @param {number} cupo - Cupo de la visita
 * @returns {Promise<Visita>} Promisa con el numero de respuesta
 */
export function agregarVisita(nombre, lugar, fecha, hora, cupo) {
    return fetchApi(`${url}/visitas`, 'POST', {
        nombre: nombre,
        lugar: lugar,
        fecha: fecha,
        hora: hora,
        cupo: cupo
    });
}

/**
 * Modifica una visita dado su id
 * 
 * Codigos de respuesta:
 *  0 si todo es correcto
 *  1 si el nombre de la visita ya existe
 *  2 si no esta logueado
 *  3 si el cupo es menor a la cantidad de inscriptos a la visita
 *
 * @param {number} id - Id del visita
 * @param {string} nombre - Nombre de la visita
 * @param {string} lugar - Lugar de la visita
 * @param {string} fecha - Fecha de la visita(año-mes-dia)
 * @param {string} hora - Hora de la visita(hora:minutos:segundos)
 * @param {number} cupo - Cupo de la visita
 * @returns {Promise<Visita>} Promisa con el codigo de respuesta
 */
export function modificarVisita(id, nombre, lugar, fecha, hora, cupo) {
    return fetchApi(`${url}/visitas/${id}`, 'PUT', {
        id: id,
        nombre: nombre,
        lugar: lugar,
        fecha: fecha,
        hora: hora,
        cupo: cupo
    });
}

/**
 * Borra una visita dado su id
 * 
 * Codigo de respuesta:
 *  0 si es correcto
 *  1 si no esta logueado
 *
 * @param {number} id - Id de la visita
 * @returns {Promise<number>} Promisa con el codigo de respuesta
 */
export function borrarVisita(id) {
    return fetchApi(`${url}/visitas/${id}`, 'DELETE', null);
}

/**
 * Obtiene todos las visitas
 * 
 * @returns {Promise<Visita>} Promisa con un arreglo de todos las visitas
 */
export function visitas() {
    return fetchApi(`${url}/visitas`, 'GET', null);
}

/**
 * Obtiene los codigos de inscripcion para imprimir
 * 
 * @returns {Promise<number[]>} Promisa con un arreglo de codigos
 */
export function codigos() {
    return fetchApi(`${url}/inscriptos/codigos`, 'GET', null);
}

/**
 * Obtiene todos los inscriptos
 *
 * @returns {Promise<Lista<Inscripto>>} Promisa con un array de alumnos
 */
export function inscriptos() {
    return fetchApi(`${url}/inscriptos`, 'GET', null);
}

/**
 * Devuelve los contadores de inscriptos
 *
 * @returns {Promise<Contadores>} Promisa con el arreglo de contadores
 */
export function contadores() {
    return fetchApi(`${url}/inscriptos/contadores`, 'GET', null);
}

/**
 * Devuelve todas las muestras
 *
 * @returns {Promise<Muestra[]>} Promisa con las muestras
 */
export function muestra() {
    const muestra = `
        query Muestra {
            muestra {
                id
                hora
                cupo
                ocupado
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: muestra,
            variables: {}
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.muestra;
    });
}

/**
 * Obtiene todos los grupos
 *
 * @returns {Promise<GruposRespuesta[]>} Promisa con los grupos
 */
export function grupos() {
    const gruposAdmin = `
        query GrupoAdmin {
            grupoAdmin {
                grupo {
                    id
                    nombre
                }
                inscriptos {
                    id
                    nombres
                    apellidos
                    celular
                }
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: gruposAdmin,
            variables: {}
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.grupoAdmin;
    });
}
