import React,{ Component } from 'react';
import {Link} from 'react-router-dom';
import ReactToPrint from 'react-to-print';
import {Consumidor} from "../../store";
import {grupos} from '../clienteGraphql.admin.js';

import "./style.scss";

class InscriptosGrupo extends Component{
    constructor(props){
        super(props);
        this.state ={
            grupos: [],
            listaActiva: [],
            grupoActivo:"",
            eleccion: 0
        }
        this.cambioLista = this.cambioLista.bind(this);
        this.universidad = this.universidad.bind(this);
    }
    cambioLista(event){
        let lista = [];
        let titulo = "";
        this.state.grupos.map((grupo,key)=>{
            if (grupo.grupo.id == this.state.eleccion){
                lista = grupo.inscriptos;
                titulo = grupo.grupo.id + "-" + grupo.grupo.nombre + "(" + grupo.inscriptos.length + ")";
                console.log(lista);
            }
        })
        this.setState({eleccion:event.target.value,listaActiva:lista,grupoActivo:titulo});
    }
    componentDidMount(){
        grupos().then((resultado)=>{
            this.setState({grupos:resultado});
            console.log(resultado);
        })
    }
    universidad(id){
        switch(id){
            case 0:
                return "sin universidad";
                break;
            case 1:
                return "UNS";
                break;
            case 2:
                return "UTN";
                break;
            case 3:
                return "otros";
                break;
        }
    }
    render(){
        return (
            <div className="home">
                <select value={this.state.eleccion} onChange={this.cambioLista}>
                    <option value="0">Elije un Grupo para ver la Lista</option>
                    {
                        this.state.grupos.map((grupo,key)=>{
                            return <option key={key} value={grupo.grupo.id}>{grupo.grupo.id}-{grupo.grupo.nombre}</option>
                        })
                    }
                </select>
                <div className="lista" ref={el => (this.componentRef = el)}>
                    <h2>{this.state.grupoActivo}</h2>
                    <table>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Celular</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.listaActiva.map((inscripto,key)=>{
                                return <tr key={key}>
                                    <td>{inscripto.id}</td>
                                    <td>{inscripto.nombres}</td>
                                    <td>{inscripto.apellidos}</td>
                                    <td>{inscripto.celular}</td>
                                </tr>
                            })
                        }
                        </tbody>
                    </table>
                    
                </div>
                <div className="fila">
                    <ReactToPrint
		                trigger={() => <button>Imprimir</button>}
		                content={() => this.componentRef}
		            />
                    <Link to="/home">Volver</Link>
                </div>
            </div>
        );
    }
}

export default Consumidor(InscriptosGrupo);