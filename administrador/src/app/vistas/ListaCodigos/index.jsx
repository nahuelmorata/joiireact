import React,{ Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import ReactToPrint from 'react-to-print';

import "./style.scss";
import {Consumidor} from "../../store";
import {codigos} from '../clienteGraphql.admin.js';


class ListaCodigos extends Component{
	constructor(props){
		super(props);
		this.state = {
			codigos: [],
		}
		this.myRef = React.createRef();
		this.imprimir = this.imprimir.bind(this);
	}
	imprimir(){
		const node = this.myRef.current;
		node.print();
	}
	componentWillMount(){
		codigos().then(resultado=>{
			this.setState({codigos:resultado})
		})
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		return(
			<div className="home">
				<div className="lista" ref={el => (this.componentRef = el)}>
					<ul>
						<li>cantidad - codigo</li>
					{
						this.state.codigos.map((codigo,index)=>{
							return <li key={index}>{index+1} - {codigo}</li>
						})
					}
					</ul>
				</div>
				<div className="fila">
				<ReactToPrint
		          trigger={() => <button>Imprimir</button>}
		          content={() => this.componentRef}
		        />
		        <Link to="/home">volver</Link>
				</div>
			</div>
			);
	}
}

export default Consumidor(ListaCodigos);
