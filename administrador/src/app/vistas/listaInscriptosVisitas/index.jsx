
import React,{ Component } from 'react';
import {Link} from 'react-router-dom';
import ReactToPrint from 'react-to-print';
import {Consumidor} from "../../store";
import {filtroInscriptoVisita,visitas} from '../clienteGraphql.admin.js';

import "./style.scss";

class InscriptosVisita extends Component{
    constructor(props){
        super(props);
        this.state ={
            visitas: [],
            visitaActiva:{},
            eleccion: 0,
            listaActiva: []
        }
        this.cambioLista = this.cambioLista.bind(this);
        this.universidad = this.universidad.bind(this);
    }
    cambioLista(event){
        let lista = [];
        let value = event.target.value;
        let token = this.props.context.estado.token;
        this.setState({eleccion:value});
        filtroInscriptoVisita(token,value).then((resultado)=>{
            lista = resultado;
        }).then(()=>{
            let index = this.state.visitas.findIndex((visita)=>{
                return visita.id == value;
            })
            this.setState({listaActiva:lista,visitaActiva:this.state.visitas[index]});
            console.log(this.state.visitaActiva);
        })
    }
    componentDidMount(){
        let lista = [];
        visitas().then((resultado)=>{
            resultado.map((visita,key)=>{
                lista.push({nombre: visita.nombre, id: visita.id, cantidad: visita.ocupado });
            });
        }).then(()=>{
                this.setState({visitas:lista});
            })
    }
    universidad(id){
        switch(id){
            case 0:
                return "sin universidad";
                break;
            case 1:
                return "UNS";
                break;
            case 2:
                return "UTN";
                break;
            case 3:
                return "otros";
                break;
        }
    }
    render(){
        return (
            <div className="home">
                <select value={this.state.eleccion} onChange={this.cambioLista}>
                    <option value="0">Elije una Visita para ver la Lista</option>
                    {
                        this.state.visitas.map((visita,key)=>{
                            return <option key={key} value={visita.id}>{visita.nombre}</option>
                        })
                    }
                </select>
                <div className="lista" ref={el => (this.componentRef = el)}>
                    
                        <h1>{this.state.visitaActiva.nombre}({this.state.visitaActiva.cantidad})</h1>
                        <table>
                        <tr>
                            <th>Documento</th>
                            <th>Apellido</th>
                            <th>Nombre</th>
                            <th>Universidad</th>
                            <th>Carrera</th>
                            <th>Lu</th>
                            <th>Email</th>
                            <th>Telefono</th>
                        </tr>
                        {
                            this.state.listaActiva.map((inscripto,key)=>{
                                return <tr key={key}>
                                     <td>{inscripto.dni}</td>
                                     <td>{inscripto.apellidos}</td>
                                     <td>{inscripto.nombres}</td>
                                     <td>{this.universidad(inscripto.universidad)}</td>
                                     <td>{inscripto.carrera}</td>
                                     <td>{inscripto.lu}</td>
                                     <td>{inscripto.email}</td>
                                     <td>{inscripto.celular}</td>
                                </tr>
                            })
                        }
                        </table>
                </div>
                <div className="fila">
                    <ReactToPrint
		                trigger={() => <button>Imprimir</button>}
		                content={() => this.componentRef}
		            />
                    <Link to="/home">Volver</Link>
                </div>
            </div>
        );
    }
}

export default Consumidor(InscriptosVisita);