import React, { Component, createContext } from 'react';
import { Redirect } from 'react-router-dom';
import { obtenerEstado } from '../vistas/clienteGraphql.admin';


const {Provider,Consumer} = createContext();

export const Consumidor = (Component) => {
	return function Componente(props){
		return(
			<Consumer>
			{({estado,acciones})=><Component {...props} context={{estado,acciones}} />}
			</Consumer>
		);
	}
}



export default class Contexto extends Component{
	constructor(props){
		super(props);
		this.state = {
			token: localStorage.getItem("token") ?? "",
			nivel: parseInt(localStorage.getItem("nivel") ?? "-1"),
			respuesta: -1
		}

		if (this.state.token) {
			void obtenerEstado();
		}
	}
	login(token, nivel){
		localStorage.setItem("token", token);
		localStorage.setItem("nivel", nivel)
		this.setState({token: token, nivel: nivel});
	}
	render(){
		var estado = {
			estado: this.state,
			acciones: {
				login: this.login.bind(this)
			}
		}
		return(
			<Provider value={estado}>
				{this.props.children}
			</Provider>
		);
	}
}
