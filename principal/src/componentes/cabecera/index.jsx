import React,{Component} from 'react';
import {NavLink} from 'react-router-dom';

import style from "./style.scss";

import logo from '../../assets/logo.png';

class Cabecera extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
        this.toggle = this.toggle.bind(this);
    }
    toggle(){
        this.setState({abierto:!this.state.abierto});
    }
    render(){
        return (
            <header className={style.cabecera}>
                <img src={logo} />
                <ul>
                    <li> <NavLink to="/" exact={true}
                    activeStyle={{
                        fontWeight: "bold",
                        color: "#eeca2c"
                      }}
                    > Inicio </NavLink> </li>
                    <li> <NavLink to="/somos"
                    activeStyle={{
                        fontWeight: "bold",
                        color: "#eeca2c"
                      }}
                    > Sobre Nosotros </NavLink> </li>
                    <li> <NavLink to="/galeria"
                    activeStyle={{
                        fontWeight: "bold",
                        color: "#eeca2c"
                      }}
                    > Galeria </NavLink> </li>
                    <li> <NavLink to="/contacto"
                    activeStyle={{
                        fontWeight: "bold",
                        color: "#eeca2c"
                      }}
                    > Contacto </NavLink> </li>
                    <li> <NavLink to="/seminarios"
                    activeStyle={{
                        fontWeight: "bold",
                        color: "#eeca2c"
                      }}
                    > Seminarios </NavLink> </li>
                </ul>
            </header>
        );
    }
}

export default Cabecera;
