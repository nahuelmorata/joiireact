import React,{Component} from 'react';

import style from "./style.scss";

class Pie extends Component{
    constructor(props){
        super(props);
        this.state = {
            fecha: '2019'
        }
        this.toggle = this.toggle.bind(this);
    }
    toggle(){
        this.setState({abierto:!this.state.abierto});
    }
    render(){
        let d = new Date();
        let fecha = d.getFullYear();
        return (
            <footer className={style.pie}>
                JOII {fecha} - Creado Por Nahuel Morata y Carlos Fernandez.
            </footer>
        );
    }
}

export default Pie;
