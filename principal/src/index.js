import React,{Component} from 'react';
import {render} from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import './normalize.scss';
import style from './style.scss';

import Cabecera from './componentes/cabecera';
import Pie from './componentes/pie';
import Inicio from './vistas/Inicio';
import Somos from './vistas/Somos';
import SomosEmpresas from './vistas/SomosEmpresas';
import SomosInscriptos from './vistas/SomosInscriptos';
import Galeria from './vistas/Galeria';
import Contacto from './vistas/Contacto';
import SeminariosVista from './vistas/SeminariosVista/index';

class App extends Component{
	constructor(props){
		super(props);
	}
	render() {
		return (
			<div className={style.contenedor} >
				<Router>
					<Cabecera />
					<Switch>
						<Route path="/" exact component={Inicio} />
						<Route path="/somos" exact component={Somos} />
						<Route path="/somos/empresas" exact component={SomosEmpresas} />
						<Route path="/somos/participantes" exact component={SomosInscriptos} />
						<Route path="/galeria" exact component={Galeria} />
						<Route path="/contacto" exact component={Contacto} />
						<Route path="/seminarios" exact component={SeminariosVista} />
					</Switch>
					<Pie />
				</Router>
			</div>
		);
	}
}

render(<App />,document.getElementById('root'));