import React,{Component} from 'react';
import {Link} from 'react-router-dom';

import style from "./style.scss";

class Vista extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }
    
    render(){
        return (
            <div className={style.contenido}>
                <h1>Sobre Nosotros</h1>
                <div className={style.selector}>
                    <Link to="/somos/participantes" className={style.seleccion}>
                        <h2>Para Alumnos</h2>
                        <img src="http://www.ecodias.com.ar/sites/default/files/imagenes-de-articulos/1/10%20Presupuesto%20Uns%20y%20bicicletas.jpg" />
                    </Link>
                    <Link to="/somos/empresas" className={style.seleccion}>
                        <h2>Para Empresas</h2>
                        <img src="http://inbahiablanca.info/uploads/unipar-bahia-interna.jpg" />
                    </Link>
                </div>
            </div>
        );
    }
}

export default Vista;
