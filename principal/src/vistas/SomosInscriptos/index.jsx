import React,{Component} from 'react';
import {Link} from 'react-router-dom';

import style from "./style.scss";

class Vista extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }
    
    render(){
        return (
            <div className={style.contenido}>
                <h1>Para Participantes</h1>
                <div className={style.info}>
                    <div>
                        <img src="https://scontent.fbhi1-1.fna.fbcdn.net/v/t1.0-9/34448693_458001764651722_3005732424295383040_o.jpg?_nc_cat=107&_nc_ht=scontent.fbhi1-1.fna&oh=44ab860fa117d5da159b098031e3a04c&oe=5D9ACE51" />
                    </div>
                    <div>
                        <h3>¿Qué son las JOII?</h3>
                        <p>Las JOII son las Jornadas de Integración de las Ingenierías, se realizan una vez por año en la ciudad de Bahía Blanca y están dirigidas a estudiantes de ingeniería y carreras afines. Las mismas ofrecen la posibilidad de adquirir nuevos conocimientos para contribuir con el desarrollo académico y ayudan a fomentar el vínculo entre todos los estudiantes, favoreciendo un encuentro interdisciplinario.</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Vista;
