import React,{Component} from 'react';
import {Link} from 'react-router-dom';

import style from "./style.scss";

class Vista extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }
    
    render(){
        return (
            <div className={style.contenido}>
                <h1>Para Empresas</h1>
                <div className={style.info}>
                    <div>
                        <img src="https://scontent.fbhi1-1.fna.fbcdn.net/v/t1.0-9/34535885_457828038002428_8154462643768262656_o.jpg?_nc_cat=101&_nc_ht=scontent.fbhi1-1.fna&oh=8cd5029d51dd588a45a5250098abdb12&oe=5D903BAA" />
                    </div>
                    <div>
                        <h3>¿Porqué sponsorear las JOII? </h3>
                        <p>Las Jornadas de Integración de las Ingenierías son una iniciativa que pretende generar un espacio de formación complementaria para estudiantes de ingeniería y carreras afines. Los asistentes tienen la posibilidad de participar de dos seminarios de interés general, podrán asistir a una visita a empresas de la Ciudad y podrán presenciar dos conferencias magistrales. Todas estas actividades están orientadas específicamente a temas ingenieriles, con el objetivo de fortalecer la formación académica. Por último, para darle un cierre a las Jornadas, se realizará una cena donde se agasajará a los invitados y podrán gozar de un ambiente distendido.</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Vista;

