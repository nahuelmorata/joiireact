import React,{Component,Fragment} from 'react';
import axios from 'axios';
import {Redirect} from 'react-router-dom';

import style from "./style.scss";

class Vista extends Component{
    constructor(props){
        super(props);
        this.state = {
            token:"5483768181.e54e9e6.2d5aa18b520541da9be51e94660da0a7",
            posts:[],
            activo:{
                imagen: '',
                texto: '',
                log:false
            }
        }
        this.elegir = this.elegir.bind(this);
        this.cerrar = this.cerrar.bind(this);
    }
    componentDidMount(){
        let vm = this;
        axios.get("https://api.instagram.com/v1/users/self/media/recent/?access_token="+this.state.token)
        .then(function (response) {
            // handle success
            console.log(response.data);
            vm.setState({posts:response.data.data});
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }
    elegir(post){
        this.setState({
            log: true,
            activo:{
                imagen:post.images.standard_resolution.url,
                texto: post.caption.text
            }
        });
    }
    cerrar(){
        this.setState({
            log: false,
            activo:{
                imagen:"",
                texto: ""
            }
        });
    }
    render(){
        let aMostrar = <Fragment></Fragment>;
        if (this.state.log){
            aMostrar =
                <div className={style.modal} onClick={this.cerrar}>
                    <img src={this.state.activo.imagen}/>
                    <h2>{this.state.activo.texto}</h2>
                </div>;
        }
        return (
            <div className={style.contenido}>
                <h1>Galeria</h1>
                <div className={style.data}>
                    {
                        this.state.posts.slice(0,18).map((post,key)=>{
                            return <img key={key} src={post.images.standard_resolution.url} onClick={()=>this.elegir(post)} />
                        })
                    }
                </div>
                { aMostrar }
            </div>
        );
    }
}

export default Vista;
