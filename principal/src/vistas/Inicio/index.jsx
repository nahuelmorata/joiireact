import React,{Component} from 'react';

import style from "./style.scss";

class Vista extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }
    render(){
        return (
            <div className={style.contenido}>
                <div className={style.data}>
                    <div className={style.texto}>
                        <h1>
                            jornadas de integración de las ingenierías
                        </h1>
                        <h2>bahía blanca - buenos aires</h2>
                    </div>
                    <div className={style.noticias}>
                        <a className={style.noticia} href="https://www.lanueva.com/nota/2019-5-26-7-0-46-mas-de-300-alumnos-de-ingenieria-se-juntan-para-resolver-problematicas-locales?fbclid=IwAR2VRG0Tq3MyTNVUZfDe2JmJdCbU1ZJeMg8tepluSjhaInXcN6DC8GBBr30">
                            <img src="https://px.cdn.lanueva.com/052019/1557931291089.jpg"/>
                            <h3>Más de 300 alumnos de Ingeniería se juntan para resolver problemáticas locales</h3>
                        </a>
                    </div>
                    <img src="https://scontent.fbhi1-1.fna.fbcdn.net/v/t1.0-9/34712626_457828238002408_2758176535193059328_o.jpg?_nc_cat=109&_nc_ht=scontent.fbhi1-1.fna&oh=30147484b0668a99e7b55e792c319777&oe=5D5462AD" />
                </div>
            </div>
        );
    }
}

export default Vista;
