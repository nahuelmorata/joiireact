import React,{Component} from 'react';

import style from "./style.scss";

class Vista extends Component{
    constructor(props){
        super(props);
        this.state = {
            nombre:"",
            email:"",
            mensaje:""
        }
        this.cambioInput = this.cambioInput.bind(this);
        this.enviarFormulario = this.enviarFormulario.bind(this);
    }
    cambioInput(event,input){
        switch(input){
            case 0:
                this.setState({nombre:event.target.value});
                break;
            case 1:
                    this.setState({email:event.target.value});
                    break;
            case 2:
                    this.setState({mensaje:event.target.value});
                    break;
        }
    }
    enviarFormulario(event){
        console.log(this.state);
        event.preventDefault();
    }
    render(){
        return (
            <div className={style.contenido}>
                <div className={style.data}>
                    
                    <form className={style.formulario} onSubmit={this.enviarFormulario}>
                        <h3>Contactate con nosotros</h3>
                        <input type="text" placeholder="Nombre" value={this.state.nombre} onChange={(e)=>this.cambioInput(e,0)}/>
                        <input type="email" placeholder="Email" value={this.state.email} onChange={(e)=>this.cambioInput(e,1)}/>
                        <textarea placeholder="Mensaje" value={this.state.mensaje} onChange={(e)=>this.cambioInput(e,2)}>
                        </textarea>
                        <button type="submit">Enviar</button>
                    </form> 
                    <div className={style.formulario}>
                        <h3>Nuestras Redes</h3>
                        <a href="#">Facebook</a>
                        <a href="#">Instagram</a>
                    </div>
                </div>   
            </div>
        );
    }
}

export default Vista;
