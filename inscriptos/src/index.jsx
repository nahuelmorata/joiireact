import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Route,Switch } from "react-router-dom";
import ReactTooltip from 'react-tooltip';

import "./style.scss";
import logo from "./app/vistas/assets/LogoJOII.png";

import Contexto from "./app/store";
//Vistas
import Login from "./app/vistas/Login";
import Home from "./app/vistas/Home";
import EditarVisitas from "./app/vistas/EditarVisitas";
import EditarSeminarios from "./app/vistas/EditarSeminarios";
import EditarDatos from "./app/vistas/EditarDatos";


render(
	<div className="contenedor-centro">
		<img src={logo} />
		<Contexto>
		<Router>
				<Switch>
					<Route path="/" exact component={Login} />
					<Route path="/home" exact component={Home} />
					<Route path="/editarVisitas" exact component={EditarVisitas} />
					<Route path="/editarSeminarios" exact component={EditarSeminarios} />
					<Route path="/editarDatos" exact component={EditarDatos} />
				</Switch>
		</Router>
		</Contexto>
		<p>Creado por <a href="mailto:contacto@nahuelmorata.com.ar" className="nada" data-tip="contacto@nahuelmorata.com.ar">Nahuel Morata</a> y <a href="mailto:charlie.jye@gmail.com" className="nada" data-tip="charlie.jye@gmail.com">Carlos Fernandez</a>.</p>
		<ReactTooltip place="top" type="dark" effect="solid"/>
	</div>
	,document.getElementById("app")
)