import React,{ Component } from 'react';
import { Redirect,Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';

import "./style.scss";
import 'react-toastify/dist/ReactToastify.min.css';
import {Consumidor} from "../../store";
import {visitas,inscripto,agregarVisitaMuestraInscripto,muestra} from '../clienteGraphql.user.js';

class EditarVisita extends Component{
	constructor(props){
		super(props);
		this.state = {
			visitas: [],
			muestras: [],
			lugar:"",
			fechaHora:"",
			fechaHoraM:"",
			visitaSeleccionada: null,
			muestraSeleccionada: null,
			guardoCorrecto: false
		}
		this.cambiarVisita = this.cambiarVisita.bind(this);
		this.cambiarMuestra = this.cambiarMuestra.bind(this);
		this.guardarCambios = this.guardarCambios.bind(this);
	}
	componentDidMount(){
		inscripto().then(resultado=>{
			this.setState({
				visitaSeleccionada: resultado.visita,
				muestraSeleccionada: resultado.muestra,
				lugar: resultado.visita?.lugar ?? "",
				fechaHora: resultado.visita?.fechaHora ?? "",
				fechaHoraM: resultado.muestra?.fechaHora ?? ""
			});
		})
		visitas().then(resultado => {
			this.setState({
				visitas: resultado
			});
		})
		muestra().then(resultado => {
			this.setState({
				muestras: resultado.items
			});
		})
	}
	cambiarVisita(event){
		let valor = event.target.value;
		const visita = this.state.visitas.find(visita => visita.id === parseInt(valor));
		this.setState({
			visitaSeleccionada: visita,
			lugar: visita.lugar,
			fechaHora: visita.fechaHora,
		});

	}
	cambiarMuestra(event){
		let valor = event.target.value;
		const muestra = this.state.muestras.find(muestra => muestra.id === parseInt(valor));
		console.log(muestra);
		this.setState({
			muestraSeleccionada: muestra,
			fechaHoraM: muestra.fechaHora
		});
	}
	guardarCambios() {
		let visita = this.state.visitaSeleccionada;
		agregarVisitaMuestraInscripto(visita, this.state.muestraSeleccionada).then(() => {
			toast("Los cambios se guardaron correctamente", {
				onClose: () => {
					this.setState({ guardoCorrecto: true });
				}
			});
		}).catch(ex => {
			toast(ex);
		});
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		if (this.state.guardoCorrecto){
			return <Redirect to="/home" />
		}
		return(
		<div className="home">
			<h1>Editar Visitas y Muestras</h1>
			<div className="fila">
			<select value={this.state.visitaSeleccionada?.id ?? 0} onChange={this.cambiarVisita} >
			<option value="0" hidden>Selecciona una Visita</option>
			{
				this.state.visitas.map((v,key)=>{
					if (v.ocupado >= v.cupo){
						return <option key={key} value={v.id}>{v.nombre}(LLENO)</option>
					} else {
						return <option key={key} value={v.id}>{v.nombre}({v.ocupado}/{v.cupo})</option>
					}
				})
			}
			</select>
			<select value={this.state.muestraSeleccionada?.id ?? 0} onChange={this.cambiarMuestra} >
			<option value="0" hidden>Selecciona una Muestra</option>
			{
				this.state.muestras.map((v,key)=>{
					if (v.ocupado >= v.cupo){
						return <option key={key} value={v.id}>{v.nombre}(LLENO)</option>
					} else {
						return <option key={key} value={v.id}>{v.hora}({v.ocupado}/{v.cupo})</option>
					}
				})
			}
			</select>
			</div>
			<div className="fila">
				<p><b>Visita:</b><br/>Lugar: {this.state.lugar}<br/>Fecha y Hora: {this.state.fechaHora}</p>
			</div>
			<div className="fila">
				<button onClick={this.guardarCambios}>Guardar Cambios</button>
				<Link to="/home">Volver</Link>
			</div>
			<ToastContainer position={toast.POSITION.TOP_RIGHT} autoClose={2000} />
		</div>
		);
	}
}

export default Consumidor(EditarVisita);
