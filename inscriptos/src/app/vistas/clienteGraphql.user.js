const url = "http://localhost:5000";

/**
 * @typedef {Object} Grupo - Representa un grupo
 * @property {number} id - Id del grupo
 * @property {string} nombre - Nombre del grupo
 */

/**
 * @typedef {Object} Inscripto - Representa un inscripto
 * @property {string} nombres - Nombres del inscripto
 * @property {string} apellidos - Apellidos del inscripto
 * @property {number} universidad - Id de la universidad del inscripto
 * @property {number} lu - LU del inscripto
 * @property {number} carrera - Id de la carrera del inscripto
 * @property {string} email - Email del inscripto
 * @property {string} celular - Celular del inscripto
 * @property {string} fechaNacimiento - Fecha de nacimiento del inscripto
 * @property {boolean} fiesta - Si va a la fiesta el inscripto
 * @property {boolean} vegetariano - Si es vegetariano el inscripto
 * @property {boolean} celiaco - Si es celiaco el inscripto
 * @property {boolean} vegano - Si es vegano el inscripto
 * @property {boolean} certificado - Si quiere el certificado el inscripto
 * @property {Seminario} seminario - Seminario obligatorio
 * @property {Seminario} seminarioOpcional - Seminario opcional
 * @property {Conferencia} conferencia - Conferencia
 * @property {Muestra} muestra - Muestra
 * @property {Grupo} grupo - Grupo al cual pertenece el inscripto
 * @property {Visita | null} visita - Visita a la cual va el inscripto
 */

/**
 * @typedef {Object} Seminario - Representa un seminario
 * @property {number} id - Id del seminario
 * @property {string} nombre - Nombre del seminario
 * @property {string} lugar - Lugar del seminario
 * @property {string} fechaHora - Fecha y hora del seminario en formato (año-mes-dia hora:minutos:segundos)
 * @property {number} cupo - Cupo del seminario
 * @property {number} ocupado - Parte ocupada del seminario
 */

/**
 * @typedef {Object} Visita - Representa una visita
 * 
 @property {number} id - Id de la visita
 * @property {string} nombre - Nombre de la visita
 * @property {string} lugar - Lugar de la visita
 * @property {string} fechaHora - Fecha y hora de la visita en formato (año-mes-dia hora:minutos:segundos)
 * @property {number} cupo - Cupo de la visita
 * @property {number} ocupado - Parte ocupada de la visita
 */ 

/**
 * @typedef {Object} Conferencia - Representa una conferencia
 * @property {number} id - Id de la conferencia
 * @property {string} nombre - Nombre de la conferencia
 * @property {string} lugar - Lugar de la conferencia
 * @property {string} fechaHora - Fecha y hora de la conferencia en formato (año-mes-dia hora:minutos:segundos)
 * @property {number} cupo - Cupo de la conferencia
 * @property {number} ocupado - Parte ocupada de la conferencia
 */ 

/**
 * @typedef {Object} Muestra - Representa una muestra
 * @property {number} id - Id de la muestra
 * @property {string} fechaHora - Hora de la muestra en formato date en string
 * @property {number} cupo - Cupo de la muestra
 * @property {number} ocupado - Parte ocupada de la muestra
 */ 

/**
 * @typedef {Object} Carrera - Representa una carrera
 * @property {number} id - Id de la carrera
 * @property {string} nombre - Nombre de la carrera
 */

/**
 * @typedef {Object} Universidad - Representa una universidad
 * @property {number} id - Id de la carrera
 * @property {string} nombre - Nombre de la carrera
 */

/**
 * @typedef {Object} FechaFinDTO - Representa la fecha fin
 * @property {string} fechaFin - Fecha fin para ser parseado por un date
 */

/** 
 * @typedef Respuesta - Representa la respuesta del servidor
 * @property {number} error - Error si es 0 es correcto sino error de usuario/contraseña
 * @property {string} token - Token de la cuenta si error == 0
 */

/**
 * Funcion de fetch configurado para hacer request a la api
 *
 * @param url Url del endpoint
 * @param metodo Metodo del endpoint
 * @param dto DTO a enviar
 * @returns {Promise<any>}
 */
const fetchApi = (url, metodo, dto) => {
    return fetch(url, {
        method: metodo,
        mode: 'cors',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem("token")}`,
            'Content-Type': 'application/json',
        },
        body: dto == null ? null : JSON.stringify(dto)
    }).then((response) => {
        if (response.status === 401) {
            localStorage.removeItem("token");
            localStorage.removeItem("nivel");
            location.reload();
            return;
        }
        if (response.status === 400) {
            throw new Error(response.statusText);
        }
        if (metodo === 'DELETE') {
            return null;
        }
        return response.json().catch(() => null);
    }).catch(() => {

    });
}

export function obtenerEstado() {
    void fetchApi(`${url}/estado`, 'GET', null);
}


/**
 * Modificar inscripto dado su token
 * 
 * El mensaje de respuesta es vacio en el caso que todo haya salido bien y se haya podido hacer 
 * los cambios sino el mensaje contendra el problema
 *
 * @returns {Promise<string>} Promisa con el mensaje de respuesta
 */
export function modificarInscripto(inscripto) {
    return fetchApi(`${url}/inscriptos/inscripto`, 'PUT', {
        nombres: inscripto.nombres,
        apellidos: inscripto.apellidos,
        universidad: inscripto.universidad,
        lu: inscripto.lu,
        carrera: inscripto.carrera,
        email: inscripto.email,
        celular: inscripto.celular,
        fechaNacimiento: inscripto.fechaNacimiento.includes('T') ? inscripto.fechaNacimiento : `${inscripto.fechaNacimiento}T00:00:00-03`,
        fiesta: inscripto.fiesta,
        vegetariano: inscripto.vegetariano,
        celiaco: inscripto.celiaco,
        vegano: inscripto.vegano,
        certificado: inscripto.certificado
    });
}

/**
 * Login para inscriptos dado su codigo y dni
 * 
 * @param {number} codigo - Codigo del inscripto
 * @param {number} dni - DNI del inscripto
 * 
 * @returns {Promise<Respuesta>} Promisa con el resultado
 */
export function login(codigo, dni) {
    return fetch(`${url}/auth/login`, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            usuario: dni,
            password: codigo
        })
    }).then((response) => {
        if (response.status === 400) {
            return null;
        }
        return response.json();
    }).catch(() => {});
}

/**
 * Obtiene todos los seminarios obligatorios
 * 
 * @returns {Promise<Seminario[]>} Promisa con un arreglo de todos los seminarios
 */
export function seminarios() {
    return fetchApi(`${url}/seminarios`, 'GET', null);
}

/**
 * Obtiene todos las visitas
 * 
 * @returns {Promise<Visita[]>} Promisa con un arreglo de todos las visitas
 */
export function visitas() {
    return fetchApi(`${url}/visitas`, 'GET', null);
}

/**
 * Devuelve los datos del inscripto
 * 
 * @returns {Promise<Inscripto>} Promisa con los datos del inscripto
 */
export function inscripto() {
    return fetchApi(`${url}/inscriptos/inscripto`, 'GET', null);
}

/**
 * Devuelve todas las conferencias
 * 
 * @returns {Promise<Lista<Conferencia>>} Promisa con los datos de las conferencias
 */
export function conferencias() {
    return fetchApi(`${url}/conferencias`, 'GET', null);
}

/**
 * Devuelve todas las muestras
 * 
 * @returns {Promise<Lista<Muestra>>} Promisa con los datos de las muestras
 */
export function muestra() {
    return fetchApi(`${url}/muestras`, 'GET', null);
}

/**
 * Agrega el seminario obligatorio, seminario opcional y conferencia al inscripto
 *
 * @param {Seminario | null} seminarioObligatorio - Id del seminario obligatorio
 * @param {Seminario | null} seminarioOpcional - Id del seminario opcional
 * @param {Conferencia | null} conferencia - Id de la conferencia
 * @returns {Promise<void>} Promisa con el codigo de respuesta
 */
export function agregarSeminarioInscripto(seminarioObligatorio, seminarioOpcional, conferencia) {
    let promisa;
    if (seminarioObligatorio != null) {
        promisa = fetchApi(`${url}/seminarios/anotar/${seminarioObligatorio.id}?obligatorio=true`, 'POST', null);
    }
    if (seminarioOpcional != null) {
        if (promisa != null) {
            promisa = promisa.then(() =>
                fetchApi(`${url}/seminarios/anotar/${seminarioOpcional.id}?obligatorio=false`, 'POST', null)
            );
        } else {
            promisa = fetchApi(`${url}/seminarios/anotar/${seminarioOpcional.id}?obligatorio=false`, 'POST', null);
        }
    }
    if (conferencia != null) {
        if (promisa != null) {
            promisa = promisa.then(() =>
                fetchApi(`${url}/conferencias/anotar/${conferencia.id}`, 'POST', null)
            );
        } else {
            promisa = fetchApi(`${url}/conferencias/anotar/${conferencia.id}`, 'POST', null);
        }
    }
    return promisa ?? Promise.resolve();
}

/**
 * Agrega la visita al inscripto
 *
 * @param {Visita | null} visita - Visita
 * @param {Muestra | null} muestra - Muestra
 * @returns {Promise<void>}
 */
export function agregarVisitaMuestraInscripto(visita, muestra) {
    let promisa;
    if (visita != null) {
        promisa = fetchApi(`${url}/visitas/anotar/${visita.id}`, 'POST', null);
    }
    if (muestra != null) {
        if (promisa != null) {
            promisa.then(() =>
                fetchApi(`${url}/muestras/anotar/${muestra.id}`, 'POST', null)
            );
        } else {
            promisa = fetchApi(`${url}/muestras/anotar/${muestra.id}`, 'POST', null);
        }
    }
    return promisa ?? Promise.resolve();
}

/**
 * Obtiene todas las carreras
 *
 * @returns {Promise<Lista<Carrera>>} Promisa con las carreras
 */
export function carreras() {
    return fetchApi(`${url}/carreras`, 'GET', null);
}

/**
 * Obtiene todas las carreras
 *
 * @returns {Promise<Lista<Universidad>>} Promisa con las carreras
 */
export function universidades() {
    return fetchApi(`${url}/universidades`, 'GET', null);
}

/**
 * Devuelve la fecha fin de modificaciones
 *
 * @returns {Promise<FechaFinDTO>} Promisa con la fecha de fin
 */
export function fechaFin() {
    return fetchApi(`${url}/configuracionGlobal/fechaFin`, 'GET', null);
}
