import React,{ Component } from 'react';
import { Redirect,Link } from "react-router-dom";

import "./style.scss";
import {Consumidor} from "../../store";
import {
	inscripto,
	visitas,
	conferencias,
	muestra,
	fechaFin,
	seminarios
} from '../clienteGraphql.user.js';
class Home extends Component{
	constructor(props){
		super(props);
		this.state = {
			inscripto:{
				grupo: {}
			},
			visita:"",
			obligatorio:"",
			opcional:"",
			conferencia:"",
			muestra:"",
			bloqueo:false
		}
	}
	componentDidMount(){
		let d = new Date();
		fechaFin().then((respuesta)=>{
			if (respuesta.fechaFin >= d){
				this.setState({bloqueo:true});
			}
		});
		inscripto().then(inscripto=>{
			this.setState({
				inscripto
			});
		}).then(() => {
			visitas().then(visitas => {
				visitas.map(visita => {
					if (this.state.inscripto.visita?.id === visita.id){
						this.setState({
							visita: visita.nombre
						});
					}
				})
			});
			seminarios().then(seminarios => {
				seminarios.forEach(seminario => {
					if (this.state.inscripto.seminario?.id === seminario.id){
						this.setState({
							obligatorio: seminario.nombre
						});
					}
					if (this.state.inscripto.seminarioOpcional?.id === seminario.id){
						this.setState({
							opcional: seminario.nombre
						});
					}
				})
			});
			conferencias().then(conferencias => {
				conferencias.items.forEach(conferencia => {
					if (this.state.inscripto.conferencia?.id === conferencia.id){
						this.setState({
							conferencia: conferencia.nombre
						});
					}
				})
			});
			muestra().then(muestras => {
				muestras.items.forEach(muestra => {
					if (this.state.inscripto.muestra?.id === muestra.id){
						const fecha = new Date(muestra.fechaHora);
						this.setState({
							muestra: new Intl.DateTimeFormat('es-AR', {
								day: '2-digit',
								month: '2-digit',
								year: 'numeric',
								hour: '2-digit',
								minute: '2-digit'
							}).format(fecha)
						});
					}
				})
			});
		});
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		return(
		<div className="home">
			<div className="parte">
				<h1>Seminarios y Conferencias</h1>
				<div className="contenido">
					<div className="columna">
						<p><b>Seminario Obligatorio:</b> {this.state.obligatorio}<br/>
						<b>Seminario Opcional:</b> {this.state.opcional}<br/>
						<b>Conferencia:</b> {this.state.conferencia}</p>
					</div>
					{
						(this.state.bloqueo ? <p>Cambios Cerrados</p> : <Link to="/editarSeminarios"> Modificar </Link>)
					}
				</div>
			</div>
			<div className="parte">
				<h1>Visitas y Muestras</h1>
				<div className="contenido">
					<div className="columna">
						<p><b>Visita: </b>{this.state.visita}<br/>
						<b>Muestra: </b>{this.state.muestra}</p>
					</div>
					{
						(this.state.bloqueo ? <p>Cambios Cerrados</p> : <Link to="/editarVisitas"> Modificar </Link>)
					}
				</div>
			</div>
			<div className="parte">
				<h1>Datos</h1>
				<div className="contenido">
					<div className="columna">
						<p><b>Nombres: </b>{this.state.inscripto.nombres}<br/>
						<b>Apellidos: </b>{this.state.inscripto.apellidos}<br/>
						<b>Grupo: </b>{(this.state.inscripto.grupo == null ? "no tenes grupo.": this.state.inscripto.grupo.nombre)}</p>
					</div>
					{
						(this.state.bloqueo ? <p>Cambios Cerrados</p> : <Link to="/editarDatos"> Modificar </Link>)
					}
				</div>
			</div>
		</div>
		);
	}
}

export default Consumidor(Home);
