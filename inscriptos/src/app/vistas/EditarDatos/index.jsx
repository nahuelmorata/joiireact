import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';

import './style.scss';
import 'react-toastify/dist/ReactToastify.min.css';
import { Consumidor } from '../../store';
import { carreras, inscripto, modificarInscripto, universidades } from '../clienteGraphql.user.js';

class EditarDatos extends Component{
	constructor(props){
		super(props);
		this.state = {
			inscripto: null,
            carreras:[],
			universidades: [],
            agregoCorrecto: false
		}
		this.cambioNombre = this.cambioNombre.bind(this);
		this.cambioApellido = this.cambioApellido.bind(this);
		this.cambioUniversidad = this.cambioUniversidad.bind(this);
		this.cambioLu = this.cambioLu.bind(this);
		this.cambioFechaNac = this.cambioFechaNac.bind(this);
		this.cambioCelular = this.cambioCelular.bind(this);
		this.cambioFiesta = this.cambioFiesta.bind(this);
		this.cambioCeliaco = this.cambioCeliaco.bind(this);
		this.cambioCertificado = this.cambioCertificado.bind(this);
		this.cambioVegetariano = this.cambioVegetariano.bind(this);
		this.cambioVegano = this.cambioVegano.bind(this);
		this.cambioCarrera = this.cambioCarrera.bind(this);
		this.cambioEmail = this.cambioEmail.bind(this);
		this.guardarCambios = this.guardarCambios.bind(this);
	}
	cambioNombre(event){
		var valor = event.target.value;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.nombres = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioApellido(event){
		var valor = event.target.value;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.apellidos = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioUniversidad(event){
		var valor = event.target.value;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.universidad = this.state.universidades.find(universidad => universidad.id === parseInt(valor));
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioCarrera(event){
		var valor = event.target.value;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.carrera = this.state.carreras.find(carrera => carrera.id === parseInt(valor));
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioLu(event){
		var valor = event.target.value;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.lu = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioEmail(event){
		var valor = event.target.value;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.email = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioCelular(event){
		var valor = event.target.value;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.celular = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioFechaNac(event){
		var valor = event.target.value;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.fechaNacimiento = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioFiesta(event){
		var valor = event.target.checked;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.fiesta = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioVegetariano(event){
		var valor = event.target.checked;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.vegetariano = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioVegano(event){
		var valor = event.target.checked;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.vegano = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioCeliaco(event){
		var valor = event.target.checked;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.celiaco = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	cambioCertificado(event){
		var valor = event.target.checked;
		let inscriptoNuevo = this.state.inscripto;
		inscriptoNuevo.certificado = valor;
		this.setState({inscripto:inscriptoNuevo});
	}
	componentDidMount(){
		inscripto().then(resultado=>{
			this.setState({inscripto:resultado});
		})
		carreras().then(resultado=>{
			this.setState({carreras:resultado.items});
		});
		universidades().then(resultado=>{
			this.setState({universidades:resultado.items});
		});
	}
	guardarCambios(){
		let inscripto = this.state.inscripto;
		
		if (inscripto.nombres === "" || inscripto.apellidos === "" || inscripto.email === "") {
			toast("Debe llenar el nombre, apellido y email");
		} else {
			modificarInscripto(this.state.inscripto).then(resultado=>{
				toast("Cambios guardados correctamente", {
					onClose: () => {
						this.setState({agregoCorrecto:true});
					}
				});
			});
		}
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		if (this.state.agregoCorrecto){
			return <Redirect to="/home" />
		}
		if (this.state.inscripto == null) {
			return(<div></div>);
		}
		return(
		<div className="home">
			<h1>Editar Datos</h1>
			<div className="fila">
				<input type="text" placeholder="nombres" value={this.state.inscripto.nombres} onChange={this.cambioNombre} />
				<input type="text" placeholder="apellidos" value={this.state.inscripto.apellidos} onChange={this.cambioApellido} />
			</div>
			<div className="fila">
				<input type="text" placeholder="celular" value={this.state.inscripto.celular} onChange={this.cambioCelular} />
				<input type="text" placeholder="email" value={this.state.inscripto.email} onChange={this.cambioEmail} />
			</div>
			<div className="fila">
				<input type="date" format="YYYY MMMM DD" placeholder="fecha de nacimiento" value={this.state.inscripto.fechaNacimiento?.split('T')[0] ?? ''} onChange={this.cambioFechaNac} />
				<input type="text" placeholder="LU" value={(this.state.inscripto.lu === 0) ? "" : this.state.inscripto.lu} onChange={this.cambioLu} />
			</div>
			<div className="fila">
				<select value={this.state.inscripto.universidad?.id ?? '0'} onChange={this.cambioUniversidad}>
					<option value="0" hidden>Universidad</option>
					{this.state.universidades.map((universidad,key)=>{
						return <option value={universidad.id} key={key}>{universidad.nombre}</option>
					})}
				</select>
				<select value={this.state.inscripto.carrera?.id ?? '0'} onChange={this.cambioCarrera}>
					<option value="0" hidden>Carrera</option>
					{this.state.carreras.map((carrera,key)=>{
						return <option value={carrera.id} key={key}>{carrera.nombre}</option>
					})}
				</select>
			</div>
			<div className="fila">
				<div className="unidos"><p><b>Celiaco: </b></p><input type="checkbox" checked={this.state.inscripto.celiaco} onChange={this.cambioCeliaco} /></div>
				<div className="unidos"><p><b>Vegetariano: </b></p><input type="checkbox" checked={this.state.inscripto.vegetariano} onChange={this.cambioVegetariano} /></div>
			</div>
			<div className="fila">
				<div className="unidos"><p><b>Vegano: </b></p><input type="checkbox" checked={this.state.inscripto.vegano} onChange={this.cambioVegano} /></div>
			</div>
			<div className="fila">
				<div className="unidos"><p><b>Fiesta: </b></p><input type="checkbox" checked={this.state.inscripto.fiesta} onChange={this.cambioFiesta} /></div>
				<div className="unidos"><p><b>Certificado: </b></p><input type="checkbox" checked={this.state.inscripto.certificado} onChange={this.cambioCertificado} /></div>
			</div>
			<div className="fila">
				<button onClick={this.guardarCambios}>guardar cambios</button>
				<Link to="/home">volver</Link>
			</div>
			<ToastContainer position={toast.POSITION.TOP_RIGHT} autoClose={2000} />
		</div>
		);
	}
}

export default Consumidor(EditarDatos);
