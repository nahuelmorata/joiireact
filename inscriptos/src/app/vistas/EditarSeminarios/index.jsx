import React,{ Component } from 'react';
import { render } from 'react-dom';
import { Redirect,Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';

import "./style.scss";
import 'react-toastify/dist/ReactToastify.min.css';
import {Consumidor} from "../../store";
import { inscripto, conferencias, agregarSeminarioInscripto, seminarios } from '../clienteGraphql.user.js';

class EditarSeminarios extends Component{
	constructor(props){
		super(props);
		this.state = {
			opcional: [],
			obligatorio: [],
			conferencias: [],
			selOp: null,
			selOb: null,
			selCo: null,
			OpcionalFecha:"",
			OpcionalLugar:"",
			ObligatorioFecha:"",
			ObligatorioLugar:"",
			ConferenciaFecha:"",
			ConferenciaLugar:"",
			guardoCorrecto:false
		}
		this.cambiarOpcional = this.cambiarOpcional.bind(this);
		this.cambiarObligatorio = this.cambiarObligatorio.bind(this);
		this.cambiarConferencia = this.cambiarConferencia.bind(this);
		this.guardarCambios = this.guardarCambios.bind(this);
	}
	cambiarObligatorio(event){
		let value = event.target.value;
		const seminario = this.state.obligatorio.find(obligatorio => obligatorio.id === parseInt(value));
		this.setState({
			selOb: seminario,
			ObligatorioFecha: seminario.fechaHora,
			ObligatorioLugar: seminario.lugar
		});
	}

	cambiarOpcional(event){
		let value = event.target.value;
		const seminario = this.state.opcional.find(opcional => opcional.id === parseInt(value));
		if (seminario.cupo <= seminario.ocupado) {
			toast("No se puede seleccionar un seminario lleno.");
			return;
		}
		this.setState({
			selOp: this.state.opcional.find(opcional => opcional.id === parseInt(value)),
			OpcionalFecha: seminario.fechaHora,
			OpcionalLugar: seminario.lugar
		});
	}
	cambiarConferencia(event){
		let value = event.target.value;
		const conferencia = this.state.conferencias.find(conferencia => conferencia.id === parseInt(value));
		this.setState({
			selCo: conferencia,
			ConferenciaFecha: conferencia.fechaHora,
			ConferenciaLugar: conferencia.lugar
		});
	}
	guardarCambios(){
		let seminarioObligatorio = this.state.selOb;
		let seminarioOpcional = this.state.selOp;
		let conferencia = this.state.selCo;
		agregarSeminarioInscripto(seminarioObligatorio, seminarioOpcional, conferencia).then(resultado=>{
			toast("Los cambios se guardaron correctamente", {
				onClose: () => {
					this.setState({
						guardoCorrecto: true
					});
				}
			});
		}).catch((error) => {
			toast(error);
		});
	}
	componentDidMount(){
		seminarios().then(seminarios => {
			this.setState({
				obligatorio: seminarios.map(seminario => ({
					id: seminario.id,
					nombre: seminario.nombre,
					lugar: seminario.lugar,
					fechaHora: seminario.fechaHora,
					cupo: seminario.cupoObligatorio,
					ocupado: seminario.ocupadoObligatorio
				})),
				opcional: seminarios.map(seminario => ({
					id: seminario.id,
					nombre: seminario.nombre,
					lugar: seminario.lugar,
					fechaHora: seminario.fechaHora,
					cupo: seminario.cupoOpcional,
					ocupado: seminario.ocupadoOpcional
				}))
			});
		});
		conferencias().then(resultado =>
			this.setState({
				conferencias:resultado.items
			})
		);
		inscripto().then(resultado =>
			this.setState({
				selOp: resultado.seminarioOpcional,
				selOb: resultado.seminario,
				selCo: resultado.conferencia,
				ObligatorioFecha: resultado.seminario.fechaHora,
				ObligatorioLugar: resultado.seminario.lugar,
				OpcionalFecha: resultado.seminarioOpcional.fechaHora,
				OpcionalLugar: resultado.seminarioOpcional.lugar,
				ConferenciaFecha: resultado.conferencia.fechaHora,
				ConferenciaLugar: resultado.conferencia.lugar
			})
		);
	}
	render() {
		if (this.props.context.estado.token === "") {
			return <Redirect to="/" />
		}
		if (this.state.guardoCorrecto){
			return <Redirect to="/home" />
		}
		return(
		<div className="home">
			<h1>Editar Seminarios y Conferencias</h1>
			<div className="fila">
			<select value={this.state.selOb?.id ?? '0'} onChange={this.cambiarObligatorio}>
			<option value="0" hidden>Selecciona Seminario Obligatorio</option>
			{
				this.state.obligatorio.map((v,key)=>{
					return <option key={key} value={v.id}>{v.nombre}({v.ocupado}/{v.cupo})</option>
				})
			}
			</select>
			<select value={this.state.selOp?.id ?? '0'} onChange={this.cambiarOpcional}>
			<option value="0" hidden>Selecciona Seminario Opcional</option>
			{
				this.state.opcional.map((v,key)=>{
					if (v.cupo <= v.ocupado){
						return <option key={key} value={v.id}>{v.nombre}(LLENO)</option>
					} else {
						return <option key={key} value={v.id}>{v.nombre}({v.ocupado}/{v.cupo})</option>
					}
				})
			}
			</select>
			</div>
			<div className="fila">
			<select value={this.state.selCo?.id ?? '0'} onChange={this.cambiarConferencia}>
			<option value="0" hidden>Selecciona Conferencia</option>
			{
				this.state.conferencias.map((v,key)=>{
					if (v.cupo <= v.ocupado){
						return <option key={key} value={v.id}>{v.nombre}(LLENO)</option>
					} else {
						return <option key={key} value={v.id}>{v.nombre}({v.ocupado}/{v.cupo})</option>
					}
				})
			}
			</select>
			</div>
			<div className="fila">
				<p><b>Seminario Obligatorio:</b> {this.state.ObligatorioFecha} - {this.state.ObligatorioLugar}</p>
				<p><b>Seminario Opcional:</b> {this.state.OpcionalFecha} - {this.state.OpcionalLugar}</p>
				<p><b>Conferencia:</b> {this.state.ConferenciaFecha} - {this.state.ConferenciaLugar}</p>
			</div>
			<div className="fila">
				<button onClick={this.guardarCambios}>guardar cambios</button>
				<Link to="/home">volver</Link>
			</div>
			<ToastContainer position={toast.POSITION.TOP_RIGHT} autoClose={2000} />
		</div>
		);
	}
}

export default Consumidor(EditarSeminarios);
