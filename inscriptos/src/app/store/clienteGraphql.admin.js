const url = "https://api.joii.com.ar/graphql";

/**
 * @typedef {Object} Respuesta - Representa la respuesta
 * @property {number} error - Error de la respuesta 
 * @property {string} token - Token de la cuenta
 */

/**
 * @typedef {Object} Seminario - Representa un seminario
 * @property {number} id - Id del seminario
 * @property {string} nombre - Nombre del seminario
 * @property {number} cupo - Cupo del seminario
 * @property {number} ocupado - Parte ocupada del seminario
 */

/**
 * @typedef {Object} Visita - Representa un visita
 * @property {number} id - Id de la visita
 * @property {string} nombre - Nombre de la visita
 * @property {number} cupo - Cupo de la visita
 * @property {number} ocupado - Parte ocupada de la visita
 */

/**
 * @typedef {Object} Alumno - Representa datos de un alumno
 * @property {number} id - Id del alumno
 * @property {string} apellidos - Apellidos del alumno
 * @property {string} nombres - Nombres del alumno
 * @property {number} dni - DNI del alumno
 */

/**
 * Agrega un inscripto
 * 
 * Codigos de respuesta:
 *  0 si todo fue correcto
 *  1 si no esta logueado
 *  2 el codigo de inscripcion no existe
 * 
 * @param {string} token - Token de la cuenta de administrador
 * @param {number} dni - DNI del inscripto
 * @returns {Promise<number>} Promisa con el codigo de respuesta
 */
export function agregarInscripto(token, codigo, dni) {
    const agregarInscriptoMutation = `
        mutation AgregarInscripto($token: String, $codigo: Int, $dni: Int) {
            agregarInscripto(token: $token, codigo: $codigo, dni: $dni)
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: agregarInscriptoMutation,
            variables: {
                token: token,
                codigo: codigo,
                dni: dni
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.agregarInscripto;
    });
}

/**
 * Loguea un usuario devolviendo un token
 * 
 * @param {string} usuario 
 * @param {string} password 
 * @returns {Promise<Respuesta>} Promisa con la respuesta
 */
export function login(usuario, password) {
    const loginCuentaMutation = `
        mutation LoginCuenta($usuario: String, $password: String) {
            loginCuenta(usuario: $usuario, password: $password) {
                error
                token
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: loginCuentaMutation,
            variables: {
                usuario: usuario,
                password: password
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.loginCuenta;
    });
}

/**
 * Filtro para obtener todos los inscriptos por seminario
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} seminarioId - Id del seminario
 * @returns {Promise<Alumno[]>} Promisa con un array de alumnos
 */
export function filtroInscriptoSeminario(token, seminarioId) {
    const filtroInscriptoSeminarioQuery = `
        query FiltroInscriptoSeminario($token: String, $seminarioId: Int) {
            inscripto(token: $token, filtro: 0, seminarioFiltrar: $seminarioId) {
                id
                apellidos
                nombres
                dni
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: filtroInscriptoSeminarioQuery,
            variables: {
                token: token,
                seminarioId: seminarioId
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.inscripto;
    });
}

/**
 * Filtro para obtener todos los inscriptos por visita
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} visitaId - Id de la visita
 * @returns {Promise<Alumno[]>} Promisa con un array de alumnos
 */
export function filtroInscriptoVisita(token, visitaId) {
    const filtroInscriptoVisitaQuery = `
        query FiltroInscriptoVisita($token: String, $visitaId: Int) {
            inscripto(token: $token, filtro: 1, visitaFiltrar: $visitaId) {
                id
                apellidos
                nombres
                dni
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: filtroInscriptoVisitaQuery,
            variables: {
                token: token,
                visitaId: visitaId
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.inscripto;
    });
}

/**
 * Filtro para obtener todos los inscriptos por universidad
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} universidadId - Id de la universidad
 * @returns {Promise<Alumno[]>} Promisa con un array de alumnos
 */
export function filtroInscriptoUniversidad(token, universidadId) {
    const filtroInscriptoUniversidadQuery = `
        query FiltroInscriptoUniversidad($token: String, $universidadId: Int) {
            inscripto(token: $token, filtro: 2, universidadFiltrar: $universidadId) {
                id
                apellidos
                nombres
                dni
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: filtroInscriptoUniversidadQuery,
            variables: {
                token: token,
                universidadId: universidadId
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.inscripto;
    });
}

/**
 * Obtiene todos los seminarios
 * 
 * @returns {Promise<Seminario>} Promisa con un arreglo de todos los seminarios
 */
export function seminarios() {
    const todosSeminariosQuery = `
        query TodosSeminarios {
            seminario {
                id
                nombre
                cupo
                ocupado
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: todosSeminariosQuery,
            variables: {}
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.seminario;
    });
}

/**
 * Agrega un seminario dado su nombre y cupo
 * 
 * @param {string} token - Token de la cuenta
 * @param {string} nombre - Nombre del seminario
 * @param {number} cupo - Cupo del seminario
 * @returns {Promise<string>} Promisa que devuelve el mensaje de error o vacio si es correcto
 */
export function agregarSeminario(token, nombre, cupo) {
    const agregarSeminarioMutation = `
        mutation AgregarSeminario($token: String, $nombre: String, $cupo: Int) {
            agregarSeminario(token: $token, nombre: $nombre, cupo: $cupo)
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: agregarSeminarioMutation,
            variables: {
                token: token,
                nombre: nombre,
                cupo: cupo
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.agregarSeminario;
    });
}

/**
 * Modifica el seminario dado su id
 * 
 * Codigos de respuesta:
 *  0 si todo es correcto
 *  1 si el nombre del seminario ya existe
 *  2 si no esta logueado
 *  3 si el cupo es menor a la cantidad de inscriptos al seminario
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} id - Id del seminario a modificar
 * @param {string} nombre - Nombre del seminario
 * @param {number} cupo - Cupo del seminario
 * @returns {Promise<number>} Promisa con el codigo de respuesta
 */
export function modificarSeminario(token, id, nombre, cupo) {
    const modificarSeminarioMutation = `
        mutation ModificarSeminario($token: String, $id: Int, $nombre: String, $cupo: Int) {
            modificarSeminario(token: $token, id: $id, nombre: $nombre, cupo: $cupo)
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: modificarSeminarioMutation,
            variables: {
                token: token,
                id: id,
                nombre: nombre,
                cupo: cupo
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.modificarSeminario;
    });
}

/**
 * Borra un seminario dado su id
 * 
 * Codigo de respuesta:
 *  0 si es correcto
 *  1 si no esta logueado
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} id - Id del seminario
 * @returns {Promise<number>} Promisa con el codigo de respuesta
 */
export function borrarSeminario(token, id) {
    const borrarSeminarioMutation = `
        mutation BorrarSeminario($token: String, $id: Int) {
            borrarSeminario(token: $token, id: $id)
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: borrarSeminarioMutation,
            variables: {
                token: token,
                id: id
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.borrarSeminario;
    });
}

/**
 * Agrega una visita dado su nombre y cupo
 * 
 * Codigos de respuesta:
 *  0 si lo agrego correctamente
 *  1 si no esta logueado
 *  2 si el nombre de la visita ya existe
 * 
 * @param {string} token - Token de la cuenta
 * @param {string} nombre - Nombre de la visita
 * @param {number} cupo - Cupo de la visita
 * @returns {Promise<number>} Promisa con el numero de respuesta
 */
export function agregarVisita(token, nombre, cupo) {
    const agregarVisitaMutation = `
        mutation AgregarVisita($token: String, $nombre: String, $cupo: Int) {
            agregarVisita(token: $token, nombre: $nombre, cupo: $cupo)
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: agregarVisitaMutation,
            variables: {
                token: token,
                nombre: nombre,
                cupo: cupo
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.agregarVisita;
    });
}

/**
 * Modifica una visita dado su id
 * 
 * Codigos de respuesta:
 *  0 si todo es correcto
 *  1 si el nombre de la visita ya existe
 *  2 si no esta logueado
 *  3 si el cupo es menor a la cantidad de inscriptos a la visita
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} id - Id del visita
 * @param {string} nombre - Nombre de la visita
 * @param {number} cupo - Cupo de la visita
 * @returns {Promise<number>} Promisa con el codigo de respuesta
 */
export function modificarVisita(token, id, nombre, cupo) {
    const modificarVisitaMutation = `
        mutation ModificarVisita($token: String, $id: Int, $nombre: String, $cupo: Int) {
            modificarVisita(token: $token, id: $id, nombre: $nombre, cupo: $cupo)
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: modificarVisitaMutation,
            variables: {
                token: token,
                id: id,
                nombre: nombre,
                cupo: cupo
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.modificarVisita;
    });
}

/**
 * Borra una visita dado su id
 * 
 * Codigo de respuesta:
 *  0 si es correcto
 *  1 si no esta logueado
 * 
 * @param {string} token - Token de la cuenta
 * @param {number} id - Id de la visita
 * @returns {Promise<number>} Promisa con el codigo de respuesta
 */
export function borrarVisita(token, id) {
    const borrarVisitaMutation = `
        mutation BorrarVisita($token: String, $id: Int) {
            borrarVisita(token: $token, id: $id)
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: borrarVisitaMutation,
            variables: {
                token: token,
                id: id
            }
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.borrarVisita;
    });
}

/**
 * Obtiene todos las visitas
 * 
 * @returns {Promise<Visita>} Promisa con un arreglo de todos las visitas
 */
export function visitas() {
    const todasVisitasQuery = `
        query TodasVisitas {
            visita {
                id
                nombre
                cupo
                ocupado
            }
        }
    `;

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: todasVisitasQuery,
            variables: {}
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.visita;
    });
}

/**
 * Obtiene los codigos de inscripcion para imprimir
 * 
 * @returns {Promise<number[]>} Promisa con un arreglo de codigos
 */
export function codigos() {
    const codigosQuery = `
        query Codigos {
            codigo
        }
    `

    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: codigosQuery,
            variables: {}
        })
    }).then((response) => {
        return response.json();
    }).then((responseJSON) => {
        return responseJSON.data.codigo;
    });
}