import React, { Component, createContext } from 'react';
import { obtenerEstado } from '../vistas/clienteGraphql.user';


const {Provider,Consumer} = createContext();

export const Consumidor = (Component) => {
	return function Componente(props){
		return(
			<Consumer>
			{({estado,acciones})=><Component {...props} context={{estado,acciones}} />}
			</Consumer>
		);
	}
}



export default class Contexto extends Component{
	constructor(props){
		super(props);
		this.state = {
			token: localStorage.getItem("token") ?? "",
			respuesta: -1
		}
		if (this.state.token) {
			void obtenerEstado();
		}
	}
	login(token){
		localStorage.setItem("token", token);
		this.setState({token: token});
	}
	render(){
		var estado = {
			estado: this.state,
			acciones: {
				login: this.login.bind(this)
			}
		}
		return(
			<Provider value={estado}>
				{this.props.children}
			</Provider>
		);
	}
}
